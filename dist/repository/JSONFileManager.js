"use strict";

var _Reflect$construct = require("@babel/runtime-corejs3/core-js-stable/reflect/construct");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _stringify = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/json/stringify"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _get2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/get"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/getPrototypeOf"));

var _FileManager2 = _interopRequireDefault(require("./FileManager"));

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = _Reflect$construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !_Reflect$construct) return false; if (_Reflect$construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(_Reflect$construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var JSONFileManager = /*#__PURE__*/function (_FileManager) {
  (0, _inherits2["default"])(JSONFileManager, _FileManager);

  var _super = _createSuper(JSONFileManager);

  function JSONFileManager() {
    (0, _classCallCheck2["default"])(this, JSONFileManager);
    return _super.apply(this, arguments);
  }

  (0, _createClass2["default"])(JSONFileManager, [{
    key: "createFile",
    value: function createFile(fileName, model) {
      (0, _get2["default"])((0, _getPrototypeOf2["default"])(JSONFileManager.prototype), "writeFile", this).call(this, fileName, (0, _stringify["default"])(model, null, 2));
    }
  }, {
    key: "getAllData",
    value: function getAllData() {
      var _context;

      return (0, _map["default"])(_context = (0, _get2["default"])((0, _getPrototypeOf2["default"])(JSONFileManager.prototype), "getAllData", this).call(this)).call(_context, function (data) {
        return JSON.parse(data);
      });
    }
  }, {
    key: "getFile",
    value: function getFile(fileName) {
      var data = (0, _get2["default"])((0, _getPrototypeOf2["default"])(JSONFileManager.prototype), "getFile", this).call(this, fileName);

      if (!data) {
        return null;
      }

      return JSON.parse(data);
    }
  }, {
    key: "writeFile",
    value: function writeFile(fileName, model) {
      return (0, _get2["default"])((0, _getPrototypeOf2["default"])(JSONFileManager.prototype), "writeFile", this).call(this, fileName, (0, _stringify["default"])(model, null, 2));
    }
  }]);
  return JSONFileManager;
}(_FileManager2["default"]);

var _default = JSONFileManager;
exports["default"] = _default;