"use strict";

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _filter = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/filter"));

var _startsWith = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/starts-with"));

var _slice = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/slice"));

var _memoizee = _interopRequireDefault(require("memoizee"));

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty2(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context3; _forEachInstanceProperty(_context3 = ownKeys(Object(source), true)).call(_context3, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context4; _forEachInstanceProperty(_context4 = ownKeys(Object(source))).call(_context4, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

var DEFAULT_MAX_AGE = 3600 * 1000; // 1 hour

var DEFAULT_PARAMETERS = {
  maxAge: DEFAULT_MAX_AGE,
  promise: true
};
/* eslint-disable no-param-reassign */

var _default = function _default() {
  var keys = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return function (target, name, descriptor) {
    var _context, _context2;

    var formattedKeys = (0, _map["default"])(keys).call(keys, function (key) {
      return key.replace('?', '');
    });
    var keySets = (0, _concat["default"])(_context = (0, _filter["default"])(_context2 = (0, _map["default"])(keys).call(keys, function (key, index) {
      if (!(0, _startsWith["default"])(key).call(key, '?')) {
        return null;
      }

      return (0, _slice["default"])(formattedKeys).call(formattedKeys, 0, index);
    })).call(_context2, function (item) {
      return !!item;
    })).call(_context, [formattedKeys]);
    var descriptorFunction = descriptor.value;
    var memoized = (0, _memoizee["default"])(descriptorFunction, _objectSpread(_objectSpread({}, DEFAULT_PARAMETERS), config));
    descriptor.value = memoized;

    if (!target._caches) {
      target._caches = [];
    }

    target._caches.push({
      fnName: descriptorFunction.name,
      keySets: keySets,
      memoized: memoized
    });

    return descriptor;
  };
};

exports["default"] = _default;