"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _set = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/set"));

var _promise = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/promise"));

var _setTimeout2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/set-timeout"));

var _parseInt2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/parse-int"));

var _isNan = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/number/is-nan"));

var _parseInt3 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/number/parse-int"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _from = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/array/from"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _fill = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/fill"));

var _setInterval2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/set-interval"));

var _stringify = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/json/stringify"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _coapPacket = require("coap-packet");

var _bufferCrc = _interopRequireDefault(require("buffer-crc32"));

var _nullthrows = _interopRequireDefault(require("nullthrows"));

var _BufferStream = _interopRequireDefault(require("./BufferStream"));

var _CoapMessages = _interopRequireDefault(require("./CoapMessages"));

var _ProtocolErrors = _interopRequireDefault(require("./ProtocolErrors"));

var _FileTransferStore = _interopRequireDefault(require("./FileTransferStore"));

var _logger = _interopRequireDefault(require("./logger"));

/*
 *   Copyright (c) 2015 Particle Industries, Inc.  All rights reserved.
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * 
 *
 */
var logger = _logger["default"].createModuleLogger(module); //
// UpdateBegin — sent by Server to initiate an OTA firmware update
// UpdateReady — sent by Device to indicate readiness to receive firmware chunks
// Chunk — sent by Server to send chunks of a firmware binary to Device
// ChunkReceived — sent by Device to respond to each chunk, indicating the CRC of
// the received chunk data.  if Server receives CRC that does not match the chunk just sent,
// that chunk is sent again
// UpdateDone — sent by Server to indicate all firmware chunks have been sent
//


var CHUNK_SIZE = 256;
var MAX_MISSED_CHUNKS = 10;
var MAX_BINARY_SIZE = 108000; // According to the forums this is the max size for device.

var Flasher = /*#__PURE__*/function () {
  // OTA tweaks
  function Flasher(client, maxBinarySize, otaChunkSize) {
    (0, _classCallCheck2["default"])(this, Flasher);
    (0, _defineProperty2["default"])(this, "_chunk", null);
    (0, _defineProperty2["default"])(this, "_chunkSize", CHUNK_SIZE);
    (0, _defineProperty2["default"])(this, "_maxBinarySize", MAX_BINARY_SIZE);
    (0, _defineProperty2["default"])(this, "_chunkIndex", void 0);
    (0, _defineProperty2["default"])(this, "_client", void 0);
    (0, _defineProperty2["default"])(this, "_fileStream", null);
    (0, _defineProperty2["default"])(this, "_lastCrc", null);
    (0, _defineProperty2["default"])(this, "_protocolVersion", 0);
    (0, _defineProperty2["default"])(this, "_startTime", void 0);
    (0, _defineProperty2["default"])(this, "_missedChunks", new _set["default"]());
    (0, _defineProperty2["default"])(this, "_fastOtaEnabled", true);
    (0, _defineProperty2["default"])(this, "_ignoreMissedChunks", false);
    this._client = client;
    this._maxBinarySize = maxBinarySize || MAX_BINARY_SIZE;
    this._chunkSize = otaChunkSize || CHUNK_SIZE;
  }

  (0, _createClass2["default"])(Flasher, [{
    key: "startFlashBuffer",
    value: function () {
      var _startFlashBuffer = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(buffer) {
        var fileTransferStore,
            address,
            _args = arguments;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                fileTransferStore = _args.length > 1 && _args[1] !== undefined ? _args[1] : _FileTransferStore["default"].FIRMWARE;
                address = _args.length > 2 && _args[2] !== undefined ? _args[2] : '0x0';

                if (!(!buffer || buffer.length === 0)) {
                  _context.next = 5;
                  break;
                }

                logger.error({
                  deviceID: this._client.getDeviceID(),
                  err: new Error('Empty file buffer')
                }, 'Flash failed! - file is empty! ');
                throw new Error('Update failed - File was empty!');

              case 5:
                if (!(buffer && buffer.length > this._maxBinarySize)) {
                  _context.next = 8;
                  break;
                }

                logger.error({
                  deviceID: this._client.getDeviceID(),
                  err: new Error('Flash: File too large'),
                  length: buffer.length
                }, 'Flash failed! - file is too large');
                throw new Error('Update failed - File was too big!');

              case 8:
                _context.prev = 8;

                if (this._claimConnection()) {
                  _context.next = 11;
                  break;
                }

                return _context.abrupt("return");

              case 11:
                this._startTime = new Date();

                this._prepare(buffer);

                _context.next = 15;
                return this._beginUpdate(buffer, fileTransferStore, address);

              case 15:
                _context.next = 17;
                return _promise["default"].race([// Fail after 60 of trying to flash
                new _promise["default"](function (resolve, reject) {
                  (0, _setTimeout2["default"])(function () {
                    return reject(new Error('Update timed out'));
                  }, 60 * 1000);
                }), this._sendFile()]);

              case 17:
                this._cleanup();

                _context.next = 24;
                break;

              case 20:
                _context.prev = 20;
                _context.t0 = _context["catch"](8);

                this._cleanup();

                throw _context.t0;

              case 24:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[8, 20]]);
      }));

      function startFlashBuffer(_x) {
        return _startFlashBuffer.apply(this, arguments);
      }

      return startFlashBuffer;
    }()
  }, {
    key: "_prepare",
    value: function _prepare(fileBuffer) {
      var _this = this;

      // make sure we have a file,
      // open a stream to our file
      if (!fileBuffer || fileBuffer.length === 0) {
        throw new Error('Flasher: this.fileBuffer was empty.');
      } else {
        this._fileStream = new _BufferStream["default"](fileBuffer);
      }

      this._chunk = null;
      this._lastCrc = null;
      this._chunkIndex = -1; // start listening for missed chunks before the update fully begins

      this._client.on('ChunkMissed', function (packet) {
        return _this._onChunkMissed(packet);
      });
    }
  }, {
    key: "_claimConnection",
    value: function _claimConnection() {
      // suspend all other messages to the device
      if (!this._client.takeOwnership(this)) {
        throw new Error('Flasher: Unable to take ownership');
      }

      return true;
    }
  }, {
    key: "_beginUpdate",
    value: function () {
      var _beginUpdate2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(buffer, fileTransferStore, address) {
        var _this2 = this;

        var maxTries, tryBeginUpdate;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                maxTries = 3;

                tryBeginUpdate = /*#__PURE__*/function () {
                  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
                    var delay, sentStatus, packet, version;
                    return _regenerator["default"].wrap(function _callee2$(_context2) {
                      while (1) {
                        switch (_context2.prev = _context2.next) {
                          case 0:
                            if (!(maxTries < 0)) {
                              _context2.next = 2;
                              break;
                            }

                            throw new Error('Failed waiting on UpdateReady - out of retries ');

                          case 2:
                            // NOTE: this is 6 because it's double the ChunkMissed 3 second delay
                            // The 90 second delay is crazy but try it just in case.
                            delay = maxTries > 0 ? 6 : 90;
                            sentStatus = _this2._sendBeginUpdateMessage(buffer, fileTransferStore, address);
                            maxTries -= 1; // did we fail to send out the UpdateBegin message?

                            if (!(sentStatus === false)) {
                              _context2.next = 7;
                              break;
                            }

                            throw new Error('UpdateBegin failed - sendMessage failed');

                          case 7:
                            _context2.next = 9;
                            return _promise["default"].race([_this2._client.listenFor('UpdateReady',
                            /* uri */
                            null,
                            /* token */
                            null), _this2._client.listenFor('UpdateAbort',
                            /* uri */
                            null,
                            /* token */
                            null).then(function (newPacket) {
                              var failReason = '';

                              if (newPacket && newPacket.payload && newPacket.payload.length) {
                                failReason = (0, _parseInt2["default"])(!!newPacket.payload.readUInt8(0), 10).toString();
                              }

                              failReason = !(0, _isNan["default"])(failReason) ? _ProtocolErrors["default"].get((0, _parseInt3["default"])(failReason, 10)) || failReason : failReason;
                              throw new Error("aborted: ".concat(failReason));
                            }), // Try to update multiple times
                            new _promise["default"](function (resolve) {
                              (0, _setTimeout2["default"])(function () {
                                if (maxTries <= 0) {
                                  return;
                                }

                                tryBeginUpdate();
                                resolve();
                              }, delay * 1000);
                            })]);

                          case 9:
                            packet = _context2.sent;

                            if (packet) {
                              _context2.next = 12;
                              break;
                            }

                            return _context2.abrupt("return");

                          case 12:
                            maxTries = 0;
                            version = 0;

                            if (packet && packet.payload.length > 0) {
                              version = packet.payload.readUInt8(0);
                            }

                            _this2._protocolVersion = version;

                          case 16:
                          case "end":
                            return _context2.stop();
                        }
                      }
                    }, _callee2);
                  }));

                  return function tryBeginUpdate() {
                    return _ref.apply(this, arguments);
                  };
                }();

                _context3.next = 4;
                return tryBeginUpdate();

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function _beginUpdate(_x2, _x3, _x4) {
        return _beginUpdate2.apply(this, arguments);
      }

      return _beginUpdate;
    }()
  }, {
    key: "_sendBeginUpdateMessage",
    value: function _sendBeginUpdateMessage(fileBuffer, fileTransferStore, address) {
      // (MDM Proposal) Optional payload to enable fast OTA and file placement:
      // u8  flags 0x01 - Fast OTA available - when set the server can
      // provide fast OTA transfer
      // u16 chunk size. Each chunk will be this size apart from the last which
      // may be smaller.
      // u32 file size. The total size of the file.
      // u8 destination. Where to store the file
      // 0x00 Firmware update
      // 0x01 External Flash
      // 0x02 User Memory Function
      // u32 destination address (0 for firmware update, otherwise the address
      // of external flash or user memory.)
      var flags = 0; // fast ota available

      var chunkSize = this._chunkSize;
      var fileSize = fileBuffer.length;
      var destFlag = fileTransferStore;
      var destAddr = (0, _parseInt2["default"])(address, 10);

      if (this._fastOtaEnabled) {
        logger.info(this._getLogInfo(), 'fast ota enabled! ');
        flags = 1;
      } // UpdateBegin — sent by Server to initiate an OTA firmware update


      return !!this._client.sendMessage('UpdateBegin', null, null, (0, _concat["default"])(Buffer).call(Buffer, [_CoapMessages["default"].toBinary(flags, 'uint8'), _CoapMessages["default"].toBinary(chunkSize, 'uint16'), _CoapMessages["default"].toBinary(fileSize, 'uint32'), _CoapMessages["default"].toBinary(destFlag, 'uint8'), _CoapMessages["default"].toBinary(destAddr, 'uint32')]), this);
    }
  }, {
    key: "_sendFile",
    value: function () {
      var _sendFile2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4() {
        var canUseFastOTA, messageToken, message, counter;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                this._chunk = null;
                this._lastCrc = null; // while iterating over our file...
                // Chunk — sent by Server to send chunks of a firmware binary to Device
                // ChunkReceived — sent by Device to respond to each chunk, indicating the CRC
                //  of the received chunk data.  if Server receives CRC that does not match
                //  the chunk just sent, that chunk is sent again
                // send when ready:
                // UpdateDone — sent by Server to indicate all firmware chunks have been sent

                canUseFastOTA = this._fastOtaEnabled && this._protocolVersion > 0;

                if (canUseFastOTA) {
                  logger.info({
                    deviceID: this._client.getDeviceID()
                  }, 'Starting FastOTA update');
                }

                this._readNextChunk();

              case 5:
                if (!this._chunk) {
                  _context4.next = 18;
                  break;
                }

                messageToken = this._sendChunk(this._chunkIndex);

                this._readNextChunk(); // We don't need to wait for the response if using FastOTA.


                if (!canUseFastOTA) {
                  _context4.next = 10;
                  break;
                }

                return _context4.abrupt("continue", 5);

              case 10:
                _context4.next = 12;
                return this._client.listenFor('ChunkReceived', null, messageToken);

              case 12:
                message = _context4.sent;
                logger.info({
                  deviceID: this._client.getDeviceID(),
                  message: message
                }, 'Chunk Received');

                if (_CoapMessages["default"].statusIsOkay(message)) {
                  _context4.next = 16;
                  break;
                }

                throw new Error("'ChunkReceived' failed.");

              case 16:
                _context4.next = 5;
                break;

              case 18:
                if (!canUseFastOTA) {
                  _context4.next = 21;
                  break;
                }

                _context4.next = 21;
                return this._waitForMissedChunks();

              case 21:
                // Handle missed chunks. Wait a maximum of 12 seconds
                counter = 0;

              case 22:
                if (!(this._missedChunks.size > 0 && counter < 3)) {
                  _context4.next = 30;
                  break;
                }

                _context4.next = 25;
                return this._resendChunks();

              case 25:
                _context4.next = 27;
                return this._waitForMissedChunks();

              case 27:
                counter += 1;
                _context4.next = 22;
                break;

              case 30:
                _context4.next = 32;
                return this._onAllChunksDone();

              case 32:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function _sendFile() {
        return _sendFile2.apply(this, arguments);
      }

      return _sendFile;
    }()
  }, {
    key: "_resendChunks",
    value: function () {
      var _resendChunks2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6() {
        var _this3 = this;

        var missedChunks, canUseFastOTA;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                missedChunks = (0, _from["default"])(this._missedChunks);

                this._missedChunks.clear();

                canUseFastOTA = this._fastOtaEnabled && this._protocolVersion > 0;
                _context6.next = 5;
                return _promise["default"].all((0, _map["default"])(missedChunks).call(missedChunks, /*#__PURE__*/function () {
                  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(chunkIndex) {
                    var offset, messageToken, message;
                    return _regenerator["default"].wrap(function _callee5$(_context5) {
                      while (1) {
                        switch (_context5.prev = _context5.next) {
                          case 0:
                            offset = chunkIndex * _this3._chunkSize;
                            (0, _nullthrows["default"])(_this3._fileStream).seek(offset);
                            _this3._chunkIndex = chunkIndex;

                            _this3._readNextChunk();

                            messageToken = _this3._sendChunk(chunkIndex); // We don't need to wait for the response if using FastOTA.

                            if (canUseFastOTA) {
                              _context5.next = 7;
                              break;
                            }

                            return _context5.abrupt("return");

                          case 7:
                            _context5.next = 9;
                            return _this3._client.listenFor('ChunkReceived', null, messageToken);

                          case 9:
                            message = _context5.sent;

                            if (_CoapMessages["default"].statusIsOkay(message)) {
                              _context5.next = 12;
                              break;
                            }

                            throw new Error("'ChunkReceived' failed.");

                          case 12:
                          case "end":
                            return _context5.stop();
                        }
                      }
                    }, _callee5);
                  }));

                  return function (_x5) {
                    return _ref2.apply(this, arguments);
                  };
                }()));

              case 5:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function _resendChunks() {
        return _resendChunks2.apply(this, arguments);
      }

      return _resendChunks;
    }()
  }, {
    key: "_readNextChunk",
    value: function _readNextChunk() {
      if (!this._fileStream) {
        logger.error({
          deviceID: this._client.getDeviceID()
        }, 'Asked to read a chunk after the update was finished');
      }

      var chunk = this._fileStream ? this._fileStream.read(this._chunkSize) : null;
      this._chunk = chunk; // workaround for https://github.com/spark/core-firmware/issues/238

      if (chunk && chunk.length !== this._chunkSize) {
        var buffer = Buffer.alloc(this._chunkSize);
        chunk.copy(buffer, 0, 0, chunk.length);
        (0, _fill["default"])(buffer).call(buffer, 0, chunk.length, this._chunkSize);
        chunk = buffer;
        this._chunk = chunk;
      }

      this._chunkIndex += 1; // end workaround

      this._lastCrc = chunk ? _bufferCrc["default"].unsigned(chunk) : null;
    }
  }, {
    key: "_sendChunk",
    value: function _sendChunk() {
      var chunkIndex = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

      var encodedCrc = _CoapMessages["default"].toBinary((0, _nullthrows["default"])(this._lastCrc), 'crc');

      var args = [encodedCrc];

      if (this._fastOtaEnabled && this._protocolVersion > 0) {
        args.push(_CoapMessages["default"].toBinary(chunkIndex, 'uint16'));
      }

      return this._client.sendMessage('Chunk', {
        args: args
      }, null, this._chunk, this);
    }
  }, {
    key: "_onAllChunksDone",
    value: function () {
      var _onAllChunksDone2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7() {
        return _regenerator["default"].wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                if (!this._client.sendMessage('UpdateDone', null, null, null, this)) {
                  logger.warn('Flasher - failed sending updateDone message');
                }

              case 1:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function _onAllChunksDone() {
        return _onAllChunksDone2.apply(this, arguments);
      }

      return _onAllChunksDone;
    }()
  }, {
    key: "_cleanup",
    value: function _cleanup() {
      try {
        // resume all other messages to the device
        this._client.releaseOwnership(this); // release our file handle


        var fileStream = this._fileStream;

        if (fileStream) {
          fileStream.close();
          this._fileStream = null;
        }
      } catch (error) {
        throw new Error("Flasher: error during cleanup ".concat(error));
      }
    }
    /*
     * delay the teardown until 4 seconds after the last
     * chunkmissed message.
     */

  }, {
    key: "_waitForMissedChunks",
    value: function () {
      var _waitForMissedChunks2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8() {
        var _this4 = this;

        var startingChunkCount, counter;
        return _regenerator["default"].wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                if (!(this._protocolVersion <= 0)) {
                  _context8.next = 2;
                  break;
                }

                return _context8.abrupt("return");

              case 2:
                startingChunkCount = this._missedChunks.size;
                counter = 0; // poll every 500ms to see if a new chunk came in and exit this early.
                // wait a total of 5 seconds

                _context8.next = 6;
                return new _promise["default"](function (resolve) {
                  var interval = (0, _setInterval2["default"])(function () {
                    counter += 1;

                    if (startingChunkCount !== _this4._missedChunks.size) {
                      clearInterval(interval);
                      resolve();
                      return;
                    } // 200ms * 5 * 4 / 1000


                    // 200ms * 5 * 4 / 1000
                    if (counter >= 20) {
                      logger.info({
                        deviceID: _this4._client.getDeviceID()
                      }, 'Finished Waiting');
                      clearInterval(interval);
                      resolve();
                    }
                  }, 200);
                });

              case 6:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this);
      }));

      function _waitForMissedChunks() {
        return _waitForMissedChunks2.apply(this, arguments);
      }

      return _waitForMissedChunks;
    }()
  }, {
    key: "_getLogInfo",
    value: function _getLogInfo() {
      if (this._client) {
        return {
          cache_key: this._client._connectionKey || undefined,
          deviceID: this._client.getDeviceID()
        };
      }

      return {
        deviceID: 'unknown'
      };
    }
  }, {
    key: "_onChunkMissed",
    value: function _onChunkMissed(packet) {
      if (this._missedChunks.size > MAX_MISSED_CHUNKS) {
        var json = (0, _stringify["default"])(this._getLogInfo());
        throw new Error("flasher - chunk missed - device over limit, killing! ".concat(json));
      } // if we're not doing a fast OTA, and ignore missed is turned on, then
      // ignore this missed chunk.


      if (!this._fastOtaEnabled && this._ignoreMissedChunks) {
        logger.info(this._getLogInfo(), 'Ignoring missed chunk');
        return;
      }

      logger.warn(this._getLogInfo(), 'Flasher - chunk missed - recovering '); // kosher if I ack before I've read the payload?

      this._client.sendReply('ChunkMissedAck', packet.messageId || 0, null, null, this); // the payload should include one or more chunk indexes


      var payload = packet.payload;

      if (!payload) {
        logger.warn(this._getLogInfo(), 'Flasher -- empty packet');
        return;
      }

      for (var ii = 0; ii < payload.length; ii += 2) {
        try {
          this._missedChunks.add(payload.readUInt16BE());
        } catch (error) {
          logger.error({
            deviceID: this._client.getDeviceID(),
            err: error
          }, 'onChunkMissed error reading payload');
        }
      }
    }
  }]);
  return Flasher;
}();

var _default = Flasher;
exports["default"] = _default;