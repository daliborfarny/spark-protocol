"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _lastIndexOf = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/last-index-of"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _FileManager = _interopRequireDefault(require("./FileManager"));

var ServerKeyFileRepository = /*#__PURE__*/function () {
  function ServerKeyFileRepository(serverKeysDir, serverKeyFileName) {
    (0, _classCallCheck2["default"])(this, ServerKeyFileRepository);
    (0, _defineProperty2["default"])(this, "_serverKeyFileName", void 0);
    (0, _defineProperty2["default"])(this, "_fileManager", void 0);
    this._fileManager = new _FileManager["default"](serverKeysDir);
    this._serverKeyFileName = serverKeyFileName;
  }

  (0, _createClass2["default"])(ServerKeyFileRepository, [{
    key: "createKeys",
    value: function () {
      var _createKeys = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(privateKeyPem, publicKeyPem) {
        var _context;

        var extIdx, pubPemFilename;
        return _regenerator["default"].wrap(function _callee$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                extIdx = (0, _lastIndexOf["default"])(_context = this._serverKeyFileName).call(_context, '.');
                pubPemFilename = "".concat(this._serverKeyFileName.substring(0, extIdx), ".pub.pem");

                this._fileManager.createFile(this._serverKeyFileName, privateKeyPem);

                this._fileManager.createFile(pubPemFilename, publicKeyPem);

                return _context2.abrupt("return", {
                  privateKeyPem: privateKeyPem,
                  publicKeyPem: publicKeyPem
                });

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee, this);
      }));

      function createKeys(_x, _x2) {
        return _createKeys.apply(this, arguments);
      }

      return createKeys;
    }()
  }, {
    key: "getPrivateKey",
    value: function () {
      var _getPrivateKey = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
        return _regenerator["default"].wrap(function _callee2$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                return _context3.abrupt("return", this._fileManager.getFile(this._serverKeyFileName));

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee2, this);
      }));

      function getPrivateKey() {
        return _getPrivateKey.apply(this, arguments);
      }

      return getPrivateKey;
    }()
  }]);
  return ServerKeyFileRepository;
}();

var _default = ServerKeyFileRepository;
exports["default"] = _default;