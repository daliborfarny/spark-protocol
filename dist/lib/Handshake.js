"use strict";

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _promise = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/promise"));

var _setTimeout2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/set-timeout"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/toConsumableArray"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _ChunkingStream = _interopRequireDefault(require("./ChunkingStream"));

var _logger = _interopRequireDefault(require("./logger"));

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context12; _forEachInstanceProperty(_context12 = ownKeys(Object(source), true)).call(_context12, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context13; _forEachInstanceProperty(_context13 = ownKeys(Object(source))).call(_context13, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

var logger = _logger["default"].createModuleLogger(module);
/*
 Handshake protocol v1

 1) Socket opens:

 2) Server responds with 40 bytes of random data as a nonce.
 Device should read exactly 40 bytes from the socket.
 Timeout: 30 seconds.  If timeout is reached, Device must close TCP socket
 and retry the connection.

 Device appends the 12-byte STM32 Unique ID to the nonce,
 RSA encrypts the 52-byte message with the Server's public key,
 and sends the resulting 256-byte ciphertext to the Server.
 The Server's public key is stored on the external flash chip at address TBD.
 The nonce should be repeated in the same byte order it arrived (FIFO)
 and the STM32 ID should be appended in the same byte order as the memory addresses:
 0x1FFFF7E8, 0x1FFFF7E9, 0x1FFFF7EA… 0x1FFFF7F2, 0x1FFFF7F3.

 3) Server should read exactly 256 bytes from the socket.
 Timeout waiting for the encrypted message is 30 seconds.
 If the timeout is reached, Server must close the connection.

 Server RSA decrypts the message with its private key.  If the decryption fails,
 Server must close the connection.
 Decrypted message should be 52 bytes, otherwise Server must close the connection.
 The first 40 bytes of the message must match the previously sent nonce,
 otherwise Server must close the connection.
 Remaining 12 bytes of message represent STM32 ID.
 Server looks up STM32 ID, retrieving the Device's public RSA key.
 If the public key is not found, Server must close the connection.

 4) Server creates secure session key
 Server generates 40 bytes of secure random data to serve as components of a session key
 for AES-128-CBC encryption.
 The first 16 bytes (MSB first) will be the key, the next 16 bytes (MSB first)
 will be the initialization vector (IV), and the final 8 bytes (MSB first) will be the salt.
 Server RSA encrypts this 40-byte message using the Device's public key
 to create a 128-byte ciphertext.
 Server creates a 20-byte HMAC of the ciphertext using SHA1 and the 40 bytes generated
 in the previous step as the HMAC key.
 Server signs the HMAC with its RSA private key generating a 256-byte signature.
 Server sends 384 bytes to Device: the ciphertext then the signature.

 5) Release control back to the Device module
 Device creates a protobufs Hello with counter set to the uint32
 represented by the most significant 4 bytes of the IV,
 encrypts the protobufs Hello with AES, and sends the ciphertext to Server.
 Server reads protobufs Hello from socket, taking note of counter.
 Each subsequent message received from Device must have the counter incremented by 1.
 After the max uint32, the next message should set the counter to zero.

 Server creates protobufs Hello with counter set to a random uint32,
 encrypts the protobufs Hello with AES, and sends the ciphertext to Device.
 Device reads protobufs Hello from socket, taking note of counter.
 Each subsequent message received from Server must have the counter incremented by 1.
 After the max uint32, the next message should set the counter to zero.
*/


var NONCE_BYTES = 40;
var ID_BYTES = 12;
var SESSION_BYTES = 40;
var GLOBAL_TIMEOUT = 30;
var DECIPHER_STREAM_TIMEOUT = 30;

var Handshake = /*#__PURE__*/function () {
  function Handshake(cryptoManager, allowDeviceToProvidePem) {
    (0, _classCallCheck2["default"])(this, Handshake);
    (0, _defineProperty2["default"])(this, "_device", void 0);
    (0, _defineProperty2["default"])(this, "_cryptoManager", void 0);
    (0, _defineProperty2["default"])(this, "_socket", void 0);
    (0, _defineProperty2["default"])(this, "_deviceID", void 0);
    (0, _defineProperty2["default"])(this, "_useChunkingStream", true);
    (0, _defineProperty2["default"])(this, "_allowDeviceToProvidePem", false);
    this._cryptoManager = cryptoManager;
    this._allowDeviceToProvidePem = allowDeviceToProvidePem;
  }

  (0, _createClass2["default"])(Handshake, [{
    key: "start",
    value: function () {
      var _start = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(device) {
        var _this = this;

        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this._device = device;
                this._socket = device._socket;
                return _context.abrupt("return", _promise["default"].race([this._runHandshake(), this._startGlobalTimeout()])["catch"](function (error) {
                  var logInfo = {
                    cache_key: _this._device && _this._device._connectionKey,
                    deviceID: _this._deviceID || null,
                    ip: _this._socket && _this._socket.remoteAddress ? _this._socket.remoteAddress.toString() : 'unknown'
                  };
                  logger.error(_objectSpread(_objectSpread({}, logInfo), {}, {
                    err: error
                  }), 'Handshake failed');
                  throw error;
                }));

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function start(_x) {
        return _start.apply(this, arguments);
      }

      return start;
    }()
  }, {
    key: "_runHandshake",
    value: function () {
      var _runHandshake2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
        var nonce, data, _yield$this$_readDevi, deviceID, deviceProvidedPem, publicKey, _yield$this$_sendSess, cipherStream, decipherStream, handshakeBuffer;

        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this._sendNonce();

              case 2:
                nonce = _context2.sent;
                _context2.next = 5;
                return this._onSocketDataAvailable();

              case 5:
                data = _context2.sent;
                _context2.next = 8;
                return this._readDeviceHandshakeData(nonce, data);

              case 8:
                _yield$this$_readDevi = _context2.sent;
                deviceID = _yield$this$_readDevi.deviceID;
                deviceProvidedPem = _yield$this$_readDevi.deviceProvidedPem;
                this._deviceID = deviceID;
                _context2.next = 14;
                return this._getDevicePublicKey(deviceID, deviceProvidedPem);

              case 14:
                publicKey = _context2.sent;
                _context2.next = 17;
                return this._sendSessionKey(publicKey);

              case 17:
                _yield$this$_sendSess = _context2.sent;
                cipherStream = _yield$this$_sendSess.cipherStream;
                decipherStream = _yield$this$_sendSess.decipherStream;
                _context2.next = 22;
                return _promise["default"].race([this._onDecipherStreamReadable(decipherStream), this._onDecipherStreamTimeout()]);

              case 22:
                handshakeBuffer = _context2.sent;

                if (handshakeBuffer) {
                  _context2.next = 25;
                  break;
                }

                throw new Error('wrong device public keys');

              case 25:
                return _context2.abrupt("return", {
                  cipherStream: cipherStream,
                  decipherStream: decipherStream,
                  deviceID: deviceID,
                  handshakeBuffer: handshakeBuffer
                });

              case 26:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function _runHandshake() {
        return _runHandshake2.apply(this, arguments);
      }

      return _runHandshake;
    }()
  }, {
    key: "_startGlobalTimeout",
    value: function () {
      var _startGlobalTimeout2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3() {
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                return _context3.abrupt("return", new _promise["default"](function (resolve, reject) {
                  (0, _setTimeout2["default"])(function () {
                    return reject(new Error("Handshake did not complete in ".concat(GLOBAL_TIMEOUT, " seconds")));
                  }, GLOBAL_TIMEOUT * 1000);
                }));

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function _startGlobalTimeout() {
        return _startGlobalTimeout2.apply(this, arguments);
      }

      return _startGlobalTimeout;
    }()
  }, {
    key: "_onSocketDataAvailable",
    value: function () {
      var _onSocketDataAvailable2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4() {
        var _this2 = this;

        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                return _context4.abrupt("return", new _promise["default"](function (resolve, reject) {
                  var onReadable = function onReadable() {
                    try {
                      var _data = _this2._socket.read();

                      if (!_data) {
                        logger.error('onSocketData called, but no data sent.');
                        reject(new Error('onSocketData called, but no data sent.'));
                      }

                      resolve(_data);
                    } catch (error) {
                      logger.error({
                        err: error
                      }, 'Handshake: Exception thrown while processing data');
                      reject(error);
                    }

                    _this2._socket.removeListener('readable', onReadable);
                  };

                  _this2._socket.on('readable', onReadable);
                }));

              case 1:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function _onSocketDataAvailable() {
        return _onSocketDataAvailable2.apply(this, arguments);
      }

      return _onSocketDataAvailable;
    }()
  }, {
    key: "_sendNonce",
    value: function () {
      var _sendNonce2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5() {
        var nonce;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return this._cryptoManager.getRandomBytes(NONCE_BYTES);

              case 2:
                nonce = _context5.sent;

                this._socket.write(nonce);

                return _context5.abrupt("return", nonce);

              case 5:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function _sendNonce() {
        return _sendNonce2.apply(this, arguments);
      }

      return _sendNonce;
    }()
  }, {
    key: "_readDeviceHandshakeData",
    value: function () {
      var _readDeviceHandshakeData2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(nonce, data) {
        var decryptedHandshakeData, nonceBuffer, deviceIDBuffer, deviceKeyBuffer, deviceProvidedPem, deviceID;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                decryptedHandshakeData = this._cryptoManager.decrypt(data);

                if (decryptedHandshakeData) {
                  _context6.next = 3;
                  break;
                }

                throw new Error('handshake data decryption failed. ' + 'You probably have incorrect server key for device');

              case 3:
                if (!(decryptedHandshakeData.length < NONCE_BYTES + ID_BYTES)) {
                  _context6.next = 5;
                  break;
                }

                throw new Error("handshake data was too small: ".concat(decryptedHandshakeData.length));

              case 5:
                nonceBuffer = Buffer.alloc(NONCE_BYTES);
                deviceIDBuffer = Buffer.alloc(ID_BYTES);
                deviceKeyBuffer = Buffer.alloc(decryptedHandshakeData.length - (NONCE_BYTES + ID_BYTES));
                decryptedHandshakeData.copy(nonceBuffer, 0, 0, NONCE_BYTES);
                decryptedHandshakeData.copy(deviceIDBuffer, 0, NONCE_BYTES, NONCE_BYTES + ID_BYTES);
                decryptedHandshakeData.copy(deviceKeyBuffer, 0, NONCE_BYTES + ID_BYTES, decryptedHandshakeData.length);

                if (nonceBuffer.equals(nonce)) {
                  _context6.next = 13;
                  break;
                }

                throw new Error('nonces didn`t match');

              case 13:
                deviceProvidedPem = this._convertDERtoPEM(deviceKeyBuffer);
                deviceID = deviceIDBuffer.toString('hex');
                return _context6.abrupt("return", {
                  deviceID: deviceID,
                  deviceProvidedPem: deviceProvidedPem
                });

              case 16:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function _readDeviceHandshakeData(_x2, _x3) {
        return _readDeviceHandshakeData2.apply(this, arguments);
      }

      return _readDeviceHandshakeData;
    }()
    /**
    * base64 encodes raw binary into
    * "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDHzg9dPG03Kv4NkS3N0xJfU8lT1M+s9HTs75
      DE1tpwXfU4GkfaLLr04j6jFpMeeggKCgWJsKyIAR9CNlVHC1IUYeejEJQCe6JReTQlq9F6bioK
      84nc9QsFTpiCIqeTAZE4t6Di5pF8qrUgQvREHrl4Nw0DR7ECODgxc/r5+XFh9wIDAQAB"
    * then formats into PEM format:
    *
    * //-----BEGIN PUBLIC KEY-----
    * //MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDHzg9dPG03Kv4NkS3N0xJfU8lT
    * //1M+s9HTs75DE1tpwXfU4GkfaLLr04j6jFpMeeggKCgWJsKyIAR9CNlVHC1IUYeej
    * //EJQCe6JReTQlq9F6bioK84nc9QsFTpiCIqeTAZE4t6Di5pF8qrUgQvREHrl4Nw0D
    * //R7ECODgxc/r5+XFh9wIDAQAB
    * //-----END PUBLIC KEY-----
    */

  }, {
    key: "_convertDERtoPEM",
    value: function _convertDERtoPEM(buffer) {
      if (!buffer || !buffer.length) {
        return null;
      }

      var bufferString = buffer.toString('base64');

      try {
        var _context7;

        var lines = (0, _concat["default"])(_context7 = ['-----BEGIN PUBLIC KEY-----']).call(_context7, (0, _toConsumableArray2["default"])(bufferString.match(/.{1,64}/g) || []), ['-----END PUBLIC KEY-----']);
        return lines.join('\n');
      } catch (error) {
        logger.error({
          bufferString: bufferString,
          err: error
        }, 'error converting DER to PEM');
      }

      return null;
    }
  }, {
    key: "_getDevicePublicKey",
    value: function () {
      var _getDevicePublicKey2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(deviceID, deviceProvidedPem) {
        var publicKey;
        return _regenerator["default"].wrap(function _callee7$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.next = 2;
                return this._cryptoManager.getDevicePublicKey(deviceID);

              case 2:
                publicKey = _context8.sent;
                console.log('_allowDeviceToProvidePem', this._allowDeviceToProvidePem);

                if (!(deviceProvidedPem && this._allowDeviceToProvidePem)) {
                  _context8.next = 8;
                  break;
                }

                _context8.next = 7;
                return this._cryptoManager.createDevicePublicKey(deviceID, deviceProvidedPem);

              case 7:
                publicKey = _context8.sent;

              case 8:
                if (publicKey) {
                  _context8.next = 10;
                  break;
                }

                throw new Error("no public key found for device: ".concat(deviceID));

              case 10:
                if (publicKey.equals(deviceProvidedPem)) {
                  _context8.next = 12;
                  break;
                }

                throw new Error("key passed to device during handshake doesn't" + "match saved public key: ".concat(deviceID));

              case 12:
                return _context8.abrupt("return", publicKey);

              case 13:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee7, this);
      }));

      function _getDevicePublicKey(_x4, _x5) {
        return _getDevicePublicKey2.apply(this, arguments);
      }

      return _getDevicePublicKey;
    }()
  }, {
    key: "_sendSessionKey",
    value: function () {
      var _sendSessionKey2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(devicePublicKey) {
        var _this3 = this;

        var sessionKey, ciphertext, hash, signedhmac, message, addErrorCallback, decipherStream, cipherStream, chunkingIn, chunkingOut;
        return _regenerator["default"].wrap(function _callee8$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.next = 2;
                return this._cryptoManager.getRandomBytes(SESSION_BYTES);

              case 2:
                sessionKey = _context9.sent;
                // Server RSA encrypts this 40-byte message using the Device's public key to
                // create a 128-byte ciphertext.
                ciphertext = devicePublicKey.encrypt(sessionKey); // Server creates a 20-byte HMAC of the ciphertext using SHA1 and the 40
                // bytes generated in the previous step as the HMAC key.

                hash = this._cryptoManager.createHmacDigest(ciphertext, sessionKey); // Server signs the HMAC with its RSA private key generating a 256-byte
                // signature.

                _context9.next = 7;
                return this._cryptoManager.sign(hash);

              case 7:
                signedhmac = _context9.sent;
                // Server sends ~384 bytes to Device: the ciphertext then the signature.
                message = (0, _concat["default"])(Buffer).call(Buffer, [ciphertext, signedhmac], ciphertext.length + signedhmac.length);

                addErrorCallback = function addErrorCallback(stream, streamName) {
                  stream.on('error', function (error) {
                    logger.error({
                      deviceID: _this3._deviceID,
                      err: error
                    }, "Error in ".concat(streamName, " stream"));
                  });
                };

                decipherStream = this._cryptoManager.createAESDecipherStream(sessionKey);
                cipherStream = this._cryptoManager.createAESCipherStream(sessionKey);
                addErrorCallback(decipherStream, 'decipher');
                addErrorCallback(cipherStream, 'cipher');

                if (this._useChunkingStream) {
                  chunkingIn = new _ChunkingStream["default"]({
                    outgoing: false
                  });
                  chunkingOut = new _ChunkingStream["default"]({
                    outgoing: true
                  });
                  addErrorCallback(chunkingIn, 'chunkingIn');
                  addErrorCallback(chunkingOut, 'chunkingOut'); // What I receive gets broken into message chunks, and goes into the
                  // decrypter

                  this._socket.pipe(chunkingIn);

                  chunkingIn.pipe(decipherStream); // What I send goes into the encrypter, and then gets broken into message
                  // chunks

                  cipherStream.pipe(chunkingOut);
                  chunkingOut.pipe(this._socket);
                } else {
                  this._socket.pipe(decipherStream);

                  cipherStream.pipe(this._socket);
                }

                this._socket.write(message);

                return _context9.abrupt("return", {
                  cipherStream: cipherStream,
                  decipherStream: decipherStream
                });

              case 17:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee8, this);
      }));

      function _sendSessionKey(_x6) {
        return _sendSessionKey2.apply(this, arguments);
      }

      return _sendSessionKey;
    }()
  }, {
    key: "_onDecipherStreamReadable",
    value: function () {
      var _onDecipherStreamReadable2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9(decipherStream) {
        return _regenerator["default"].wrap(function _callee9$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                return _context10.abrupt("return", new _promise["default"](function (resolve) {
                  var callback = function callback() {
                    var chunk = decipherStream.read();
                    resolve(chunk);
                    decipherStream.removeListener('readable', callback);
                  };

                  decipherStream.on('readable', callback);
                }));

              case 1:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee9);
      }));

      function _onDecipherStreamReadable(_x7) {
        return _onDecipherStreamReadable2.apply(this, arguments);
      }

      return _onDecipherStreamReadable;
    }()
  }, {
    key: "_onDecipherStreamTimeout",
    value: function () {
      var _onDecipherStreamTimeout2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee10() {
        return _regenerator["default"].wrap(function _callee10$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                _context11.next = 2;
                return new _promise["default"](function (resolve, reject) {
                  (0, _setTimeout2["default"])(function () {
                    return reject();
                  }, DECIPHER_STREAM_TIMEOUT * 1000);
                });

              case 2:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee10);
      }));

      function _onDecipherStreamTimeout() {
        return _onDecipherStreamTimeout2.apply(this, arguments);
      }

      return _onDecipherStreamTimeout;
    }()
  }]);
  return Handshake;
}();

var _default = Handshake;
exports["default"] = _default;