"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _promise = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/promise"));

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _crypto = _interopRequireDefault(require("crypto"));

var _nodeRsa = _interopRequireDefault(require("node-rsa"));

var _CryptoStream = _interopRequireDefault(require("./CryptoStream"));

var _DeviceKey = _interopRequireDefault(require("./DeviceKey"));

var HASH_TYPE = 'sha1';

var CryptoManager = /*#__PURE__*/function () {
  function CryptoManager(deviceKeyRepository, serverKeyRepository, serverKeyPassword) {
    var _this = this;

    (0, _classCallCheck2["default"])(this, CryptoManager);
    (0, _defineProperty2["default"])(this, "_deviceKeyRepository", void 0);
    (0, _defineProperty2["default"])(this, "_serverKeyRepository", void 0);
    (0, _defineProperty2["default"])(this, "_serverPrivateKey", void 0);
    (0, _defineProperty2["default"])(this, "_serverKeyPassword", void 0);
    this._deviceKeyRepository = deviceKeyRepository;
    this._serverKeyRepository = serverKeyRepository;
    this._serverKeyPassword = serverKeyPassword;
    (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this._getServerPrivateKey();

            case 2:
              _this._serverPrivateKey = _context.sent;

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  }

  (0, _createClass2["default"])(CryptoManager, [{
    key: "_createCryptoStream",
    value: function _createCryptoStream(sessionKey, encrypt) {
      // The first 16 bytes (MSB first) will be the key,
      // the next 16 bytes (MSB first) will be the initialization vector (IV),
      // and the final 8 bytes (MSB first) will be the salt.
      var key = Buffer.alloc(16); // just the key... +8); //key plus salt

      var iv = Buffer.alloc(16); // initialization vector

      sessionKey.copy(key, 0, 0, 16); // copy the key

      sessionKey.copy(iv, 0, 16, 32); // copy the iv

      return new _CryptoStream["default"]({
        iv: iv,
        key: key,
        streamType: encrypt ? 'encrypt' : 'decrypt'
      });
    }
  }, {
    key: "_createServerKeys",
    value: function () {
      var _createServerKeys2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
        var privateKey;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                privateKey = new _nodeRsa["default"]({
                  b: 2048
                });
                _context2.next = 3;
                return this._serverKeyRepository.createKeys(privateKey.exportKey('pkcs1-private-pem'), privateKey.exportKey('pkcs8-public-pem'));

              case 3:
                return _context2.abrupt("return", privateKey);

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function _createServerKeys() {
        return _createServerKeys2.apply(this, arguments);
      }

      return _createServerKeys;
    }()
  }, {
    key: "_getServerPrivateKey",
    value: function () {
      var _getServerPrivateKey2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3() {
        var privateKeyString;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return this._serverKeyRepository.getPrivateKey();

              case 2:
                privateKeyString = _context3.sent;

                if (privateKeyString) {
                  _context3.next = 5;
                  break;
                }

                return _context3.abrupt("return", this._createServerKeys());

              case 5:
                return _context3.abrupt("return", new _nodeRsa["default"](privateKeyString, {
                  encryptionScheme: 'pkcs1',
                  signingScheme: 'pkcs1'
                }));

              case 6:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function _getServerPrivateKey() {
        return _getServerPrivateKey2.apply(this, arguments);
      }

      return _getServerPrivateKey;
    }()
  }, {
    key: "createAESCipherStream",
    value: function createAESCipherStream(sessionKey) {
      return this._createCryptoStream(sessionKey, true);
    }
  }, {
    key: "createAESDecipherStream",
    value: function createAESDecipherStream(sessionKey) {
      return this._createCryptoStream(sessionKey, false);
    }
  }, {
    key: "createHmacDigest",
    value: function createHmacDigest(ciphertext, sessionKey) {
      var hmac = _crypto["default"].createHmac(HASH_TYPE, sessionKey);

      hmac.update(ciphertext);
      return hmac.digest();
    }
  }, {
    key: "createDevicePublicKey",
    value: function () {
      var _createDevicePublicKey = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(deviceID, publicKeyPem) {
        var output;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                output = new _DeviceKey["default"](publicKeyPem);
                _context4.next = 3;
                return this._deviceKeyRepository.updateByID(deviceID, {
                  deviceID: deviceID,
                  key: publicKeyPem
                });

              case 3:
                return _context4.abrupt("return", output);

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function createDevicePublicKey(_x, _x2) {
        return _createDevicePublicKey.apply(this, arguments);
      }

      return createDevicePublicKey;
    }()
  }, {
    key: "decrypt",
    value: function decrypt(data) {
      try {
        return this._serverPrivateKey.decrypt(data);
      } catch (error) {
        return null;
      }
    }
  }, {
    key: "getDevicePublicKey",
    value: function () {
      var _getDevicePublicKey = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(deviceID) {
        var publicKeyObject;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return this._deviceKeyRepository.getByID(deviceID);

              case 2:
                publicKeyObject = _context5.sent;

                if (publicKeyObject) {
                  _context5.next = 5;
                  break;
                }

                return _context5.abrupt("return", null);

              case 5:
                return _context5.abrupt("return", new _DeviceKey["default"](publicKeyObject.key));

              case 6:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function getDevicePublicKey(_x3) {
        return _getDevicePublicKey.apply(this, arguments);
      }

      return getDevicePublicKey;
    }()
  }, {
    key: "getRandomBytes",
    value: function getRandomBytes(size) {
      return new _promise["default"](function (resolve, reject) {
        _crypto["default"].randomBytes(size, function (error, buffer) {
          if (error) {
            reject(error);
            return;
          }

          resolve(buffer);
        });
      });
    }
  }, {
    key: "sign",
    value: function () {
      var _sign = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(hash) {
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                return _context6.abrupt("return", this._serverPrivateKey.encryptPrivate(hash));

              case 1:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function sign(_x4) {
        return _sign.apply(this, arguments);
      }

      return sign;
    }()
  }], [{
    key: "getRandomUINT16",
    value: function getRandomUINT16() {
      // ** - the same as Math.pow()
      var uintMax = Math.pow(2, 16) - 1; // 65535

      return Math.floor(Math.random() * uintMax + 1);
    }
  }]);
  return CryptoManager;
}();

var _default = CryptoManager;
exports["default"] = _default;