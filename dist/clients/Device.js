"use strict";

var _Reflect$construct = require("@babel/runtime-corejs3/core-js-stable/reflect/construct");

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = exports.SYSTEM_EVENT_NAMES = exports.DEVICE_STATUS_MAP = exports.DEVICE_MESSAGE_EVENTS_NAMES = exports.DEVICE_EVENT_NAMES = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _promise = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/promise"));

var _setTimeout2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/set-timeout"));

var _parseFloat2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/parse-float"));

var _indexOf = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/index-of"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _values = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/object/values"));

var _some = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/some"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/getPrototypeOf"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _events = _interopRequireDefault(require("events"));

var _nullthrows = _interopRequireDefault(require("nullthrows"));

var _CoapMessage = _interopRequireDefault(require("../lib/CoapMessage"));

var _CryptoManager = _interopRequireDefault(require("../lib/CryptoManager"));

var _FileTransferStore = _interopRequireDefault(require("../lib/FileTransferStore"));

var _CoapMessages = _interopRequireDefault(require("../lib/CoapMessages"));

var _Flasher = _interopRequireDefault(require("../lib/Flasher"));

var _settings = _interopRequireDefault(require("../settings"));

var _logger = _interopRequireDefault(require("../lib/logger"));

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context14; _forEachInstanceProperty(_context14 = ownKeys(Object(source), true)).call(_context14, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context15; _forEachInstanceProperty(_context15 = ownKeys(Object(source))).call(_context15, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = _Reflect$construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !_Reflect$construct) return false; if (_Reflect$construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(_Reflect$construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var logger = _logger["default"].createModuleLogger(module);

// Hello — sent first by Device then by Server immediately after handshake, never again
// Ignored — sent by either side to respond to a message with a bad counter value.
// The receiver of an Ignored message can optionally decide to resend a previous message
// if the indicated bad counter value matches a recently sent message.
// package flasher
// Chunk — sent by Server to send chunks of a firmware binary to Device
// ChunkReceived — sent by Device to respond to each chunk,
// indicating the CRC of the received chunk data.
// if Server receives CRC that does not match the chunk just sent, that chunk is sent again
// UpdateBegin — sent by Server to initiate an OTA firmware update
// UpdateReady — sent by Device to indicate readiness to receive firmware chunks
// UpdateDone — sent by Server to indicate all firmware chunks have been sent
// FunctionCall — sent by Server to tell Device to call a user-exposed function
// FunctionReturn — sent by Device in response to FunctionCall to indicate return value.
// void functions will not send this message
// VariableRequest — sent by Server to request the value of a user-exposed variable
// VariableValue — sent by Device in response to VariableRequest to indicate the value
// Event — sent by Device to initiate a Server Sent Event and optionally
// an HTTP callback to a 3rd party
// KeyChange — sent by Server to change the AES credentials

/**
 * How high do our counters go before we wrap around to 0?
 * (CoAP maxes out at a 16 bit int)
 */
var COUNTER_MAX = 65536;
/**
 * How big can our tokens be in CoAP messages?
 */

var TOKEN_COUNTER_MAX = 256;
var KEEP_ALIVE_TIMEOUT = _settings["default"].KEEP_ALIVE_TIMEOUT;
var SOCKET_TIMEOUT = _settings["default"].SOCKET_TIMEOUT;
var DEVICE_EVENT_NAMES = {
  DISCONNECT: 'disconnect',
  FLASH_FAILED: 'flash/failed',
  FLASH_STARTED: 'flash/started',
  FLASH_SUCCESS: 'flash/success',
  READY: 'ready'
};
exports.DEVICE_EVENT_NAMES = DEVICE_EVENT_NAMES;
var SYSTEM_EVENT_NAMES = {
  APP_HASH: 'spark/device/app-hash',
  CLAIM_CODE: 'spark/device/claim/code',
  FLASH_AVAILABLE: 'spark/flash/available',
  FLASH_PROGRESS: 'spark/flash/progress',
  FLASH_STATUS: 'spark/flash/status',
  GET_IP: 'particle/device/ip',
  GET_NAME: 'particle/device/name',
  GET_RANDOM_BUFFER: 'particle/device/random',
  IDENTITY: 'spark/device/ident/0',
  LAST_RESET: 'spark/device/last_reset',
  // This should just have a friendly string in its payload.
  MAX_BINARY: 'spark/hardware/max_binary',
  OTA_CHUNK_SIZE: 'spark/hardware/ota_chunk_size',
  OTA_RESULT: 'spark/device/ota_result',
  RESET: 'spark/device/reset',
  // send this to reset passing "safe mode"/"dfu"/"reboot"
  SAFE_MODE: 'spark/device/safemode',
  SAFE_MODE_UPDATING: 'spark/safe-mode-updater/updating',
  SPARK_STATUS: 'spark/status',
  SPARK_SUBSYSTEM: 'spark/cc3000-patch-version',
  UPDATES_ENABLED: 'particle/device/updates/enabled',
  UPDATES_FORCED: 'particle/device/updates/forced'
}; // These constants should be consistent with message names in
// MessageSpecifications.js

exports.SYSTEM_EVENT_NAMES = SYSTEM_EVENT_NAMES;
var DEVICE_MESSAGE_EVENTS_NAMES = {
  GET_TIME: 'GetTime',
  PRIVATE_EVENT: 'PrivateEvent',
  PUBLIC_EVENT: 'PublicEvent',
  SUBSCRIBE: 'Subscribe'
};
exports.DEVICE_MESSAGE_EVENTS_NAMES = DEVICE_MESSAGE_EVENTS_NAMES;
var DEVICE_STATUS_MAP = {
  GOT_DESCRIPTION: 3,
  GOT_HELLO: 2,
  INITIAL: 1,
  READY: 4
};
exports.DEVICE_STATUS_MAP = DEVICE_STATUS_MAP;
var NEW_STATUS_EVENT_NAME = 'newStatus';

/**
 * Implementation of the Particle messaging protocol
 * @Device
 */
var Device = /*#__PURE__*/function (_EventEmitter) {
  (0, _inherits2["default"])(Device, _EventEmitter);

  var _super = _createSuper(Device);

  function Device(socket, connectionKey, handshake) {
    var _this;

    (0, _classCallCheck2["default"])(this, Device);
    _this = _super.call(this);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_attributes", {
      appHash: null,
      deviceID: '',
      functions: null,
      ip: 'unkonwn',
      lastHeard: null,
      name: '',
      ownerID: null,
      particleProductId: 0,
      platformId: 0,
      productFirmwareVersion: 0,
      registrar: null,
      reservedFlags: 0,
      variables: null
    });
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_attributesFromDevice", {
      particleProductId: 0,
      platformId: 0,
      productFirmwareVersion: 0,
      reservedFlags: 0
    });
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_cipherStream", null);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_connectionKey", null);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_connectionStartTime", null);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_decipherStream", null);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_disconnectCounter", 0);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_isFlashing", false);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_maxBinarySize", null);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_otaChunkSize", null);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_owningFlasher", void 0);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_receiveCounter", 0);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_sendCounter", 0);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_sendToken", 0);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_socket", void 0);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_socketTimeoutInterval", null);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_status", DEVICE_STATUS_MAP.INITIAL);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_statusEventEmitter", new _events["default"]());
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_systemInformation", void 0);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_tokens", {});
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_handshake", void 0);
    _this._connectionKey = connectionKey;
    _this._socket = socket;
    _this._handshake = handshake;
    return _this;
  } // emit(type, ...args: Array<mixed>) {
  //   const packet = args[0];
  //   const eventData =
  //     packet != null
  //       ? {
  //           connectionID: this.getConnectionKey(),
  //           deviceID: this._attributes.deviceID,
  //           name: CoapMessages.getUriPath(packet).substr(3),
  //           ttl: CoapMessages.getMaxAge(packet),
  //         }
  //       : new Error();
  //   logger.info(eventData, `Device Event: ${  type}`);
  //   super.emit(type, ...args);
  // }


  (0, _createClass2["default"])(Device, [{
    key: "getAttributes",
    value: function getAttributes() {
      return _objectSpread(_objectSpread({}, this._attributes), this._attributesFromDevice);
    }
  }, {
    key: "getStatus",
    value: function getStatus() {
      return this._status;
    }
  }, {
    key: "getSystemInformation",
    value: function getSystemInformation() {
      return (0, _nullthrows["default"])(this._systemInformation);
    }
  }, {
    key: "isFlashing",
    value: function isFlashing() {
      return this._isFlashing;
    }
  }, {
    key: "updateAttributes",
    value: function updateAttributes(attributes) {
      this._attributes = _objectSpread(_objectSpread(_objectSpread({}, this._attributes), attributes), this._attributesFromDevice);
      return this._attributes;
    }
  }, {
    key: "setMaxBinarySize",
    value: function setMaxBinarySize(maxBinarySize) {
      this._maxBinarySize = maxBinarySize;
    }
  }, {
    key: "setOtaChunkSize",
    value: function setOtaChunkSize(maxBinarySize) {
      this._otaChunkSize = maxBinarySize;
    }
  }, {
    key: "setStatus",
    value: function setStatus(status) {
      this._status = status;

      this._statusEventEmitter.emit(NEW_STATUS_EVENT_NAME, status);
    }
  }, {
    key: "hasStatus",
    value: function () {
      var _hasStatus = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(status) {
        var _this2 = this;

        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(status <= this._status)) {
                  _context.next = 2;
                  break;
                }

                return _context.abrupt("return", _promise["default"].resolve());

              case 2:
                return _context.abrupt("return", new _promise["default"](function (resolve) {
                  var deviceStatusListener = function deviceStatusListener(newStatus) {
                    if (status <= newStatus) {
                      resolve();

                      _this2._statusEventEmitter.removeListener(NEW_STATUS_EVENT_NAME, deviceStatusListener);
                    }
                  };

                  _this2._statusEventEmitter.on(NEW_STATUS_EVENT_NAME, deviceStatusListener);
                }));

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function hasStatus(_x) {
        return _hasStatus.apply(this, arguments);
      }

      return hasStatus;
    }()
    /**
     * configure our socket and start the handshake
     */

  }, {
    key: "startProtocolInitialization",
    value: function () {
      var _startProtocolInitialization = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
        var _this3 = this;

        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                this._socket.setNoDelay(true);

                this._socket.setKeepAlive(true, KEEP_ALIVE_TIMEOUT); // every 15 second(s)


                this._socket.setTimeout(SOCKET_TIMEOUT);

                this._socket.on('error', function (error) {
                  return _this3.disconnect("socket error: ".concat(error.message));
                });

                this._socket.on('close', function () {
                  return _this3.disconnect('socket close');
                });

                this._socket.on('timeout', function () {
                  return _this3.disconnect('socket timeout');
                });

                return _context2.abrupt("return", this.startHandshake());

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function startProtocolInitialization() {
        return _startProtocolInitialization.apply(this, arguments);
      }

      return startProtocolInitialization;
    }()
  }, {
    key: "startHandshake",
    value: function () {
      var _startHandshake = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3() {
        var result, cipherStream, decipherStream, deviceID, handshakeBuffer, getHelloInfo;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return this._handshake.start(this);

              case 3:
                result = _context3.sent;

                if (result) {
                  _context3.next = 6;
                  break;
                }

                throw new Error('Handshake result undefined');

              case 6:
                cipherStream = result.cipherStream, decipherStream = result.decipherStream, deviceID = result.deviceID, handshakeBuffer = result.handshakeBuffer;
                this._cipherStream = cipherStream;
                this._decipherStream = decipherStream;
                getHelloInfo = this._getHello(handshakeBuffer);
                this.updateAttributes(_objectSpread(_objectSpread({}, getHelloInfo || {}), {}, {
                  deviceID: deviceID,
                  ip: this.getRemoteIPAddress()
                }));
                this.setStatus(DEVICE_STATUS_MAP.GOT_HELLO);
                return _context3.abrupt("return", deviceID);

              case 15:
                _context3.prev = 15;
                _context3.t0 = _context3["catch"](0);
                this.disconnect(_context3.t0);
                throw _context3.t0;

              case 19:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 15]]);
      }));

      function startHandshake() {
        return _startHandshake.apply(this, arguments);
      }

      return startHandshake;
    }()
  }, {
    key: "completeProtocolInitialization",
    value: function () {
      var _completeProtocolInitialization = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4() {
        var _this4 = this;

        var decipherStream, _yield$this$_getDescr, functionState, systemInformation;

        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                decipherStream = this._decipherStream;

                if (decipherStream) {
                  _context4.next = 4;
                  break;
                }

                throw new Error('decipherStream not set.');

              case 4:
                decipherStream.on('readable', function () {
                  var read = function read() {
                    return decipherStream.read();
                  };

                  var chunk = read();

                  while (chunk !== null) {
                    _this4._clientHasWrittenToSocket();

                    _this4.routeMessage(chunk);

                    chunk = read();
                  }

                  _this4._clientHasWrittenToSocket();
                }); // Wait for this thing to be readable before sending any messages

                /* await new Promise((resolve: () => void) => {
                decipherStream.once('readable', resolve);
                }); */

                this._sendHello();

                this._connectionStartTime = new Date();
                _context4.next = 9;
                return this._getDescription();

              case 9:
                _yield$this$_getDescr = _context4.sent;
                functionState = _yield$this$_getDescr.functionState;
                systemInformation = _yield$this$_getDescr.systemInformation;
                this._systemInformation = systemInformation;
                this.updateAttributes({
                  functions: (0, _nullthrows["default"])(functionState).f,
                  variables: (0, _nullthrows["default"])(functionState).v
                });
                this.setStatus(DEVICE_STATUS_MAP.GOT_DESCRIPTION);
                logger.info({
                  cache_key: this._connectionKey,
                  deviceID: this.getDeviceID(),
                  firmwareVersion: this._attributesFromDevice.productFirmwareVersion,
                  ip: this.getRemoteIPAddress(),
                  particleProductId: this._attributesFromDevice.particleProductId,
                  platformId: this._attributesFromDevice.platformId
                }, 'On device protocol initialization complete');
                return _context4.abrupt("return", systemInformation);

              case 19:
                _context4.prev = 19;
                _context4.t0 = _context4["catch"](0);
                logger.error(_objectSpread(_objectSpread({}, this._attributes), {}, {
                  err: _context4.t0
                }), 'completeProtocolInitialization');
                throw _context4.t0;

              case 23:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 19]]);
      }));

      function completeProtocolInitialization() {
        return _completeProtocolInitialization.apply(this, arguments);
      }

      return completeProtocolInitialization;
    }() // This handles the case on some operating systems where `socket.setTimeout`
    // doesn't work. On windows, that function will timeout when if the client
    // doesn't send a reply. On Linux as long as someone is reading or writing
    // to a socket it will stay open.

  }, {
    key: "_clientHasWrittenToSocket",
    value: function _clientHasWrittenToSocket() {
      var _this5 = this;

      if (this._socketTimeoutInterval) {
        clearTimeout(this._socketTimeoutInterval);
      }

      this._socketTimeoutInterval = (0, _setTimeout2["default"])(function () {
        return _this5.disconnect('socket timeout');
      }, SOCKET_TIMEOUT);
    }
  }, {
    key: "_getHello",
    value: function _getHello(chunk) {
      var message = _CoapMessages["default"].unwrap(chunk);

      if (!message || !message.messageId) {
        throw new Error('failed to parse hello');
      }

      this._receiveCounter = message.messageId;

      try {
        var payload = message.payload;

        if (!payload || payload.length <= 0) {
          return null;
        }

        this._attributesFromDevice = {
          particleProductId: payload.readUInt16BE(0),
          platformId: payload.readUInt16BE(6),
          productFirmwareVersion: payload.readUInt16BE(2),
          reservedFlags: payload.readUInt16BE(4)
        };
        logger.info(this._attributesFromDevice, 'Connection attributes');

        if (this._attributesFromDevice.platformId !== 0) {
          // This is the maximum for Photon/P1.
          // It should be updated for Boron and other types.
          this.setMaxBinarySize(384000);
        }

        return this._attributesFromDevice;
      } catch (error) {
        logger.error({
          deviceID: this.getDeviceID(),
          err: error
        }, 'Error while parsing hello payload ');
        return null;
      }
    }
  }, {
    key: "_sendHello",
    value: function _sendHello() {
      // client will set the counter property on the message
      this._sendCounter = _CryptoManager["default"].getRandomUINT16();
      this.sendMessage('Hello');
    }
  }, {
    key: "ping",
    value: function ping() {
      if (_settings["default"].SHOW_VERBOSE_DEVICE_LOGS) {
        logger.info({
          deviceID: this.getDeviceID()
        }, 'Pinged, replying');
      }

      return {
        connected: this._socket !== null,
        lastHeard: this._attributes.lastHeard
      };
    }
    /**
     * Deals with messages coming from the device over our secure connection
     * @param data
     */

  }, {
    key: "routeMessage",
    value: function routeMessage(data) {
      var packet = _CoapMessages["default"].unwrap(data);

      if (!packet) {
        logger.error({
          deviceID: this.getDeviceID()
        }, 'RouteMessage got a NULL COAP message ');
        return;
      } // make sure the packet always has a number for code...


      var messageCode = (0, _parseFloat2["default"])(packet.code);
      packet.code = messageCode.toString(); // Get the message code (the decimal portion of the code) to determine
      // how we should handle the message

      var requestType = '';

      if (messageCode > _CoapMessage["default"].Code.EMPTY && messageCode <= _CoapMessage["default"].Code.DELETE) {
        // probably a request
        requestType = _CoapMessages["default"].getRequestType(packet);
      }

      if (!requestType) {
        requestType = this._getResponseType(packet.token || Buffer.from([]));
      } // This is just a dumb ack packet. We don't really need to do anything
      // with it.


      if (packet.ack) {
        if (!requestType) {
          // no type, can't route it.
          requestType = 'PingAck';
        }

        this.emit(requestType, packet);
        return;
      }

      this._incrementReceiveCounter();

      if (packet.code === '0' && packet.confirmable) {
      
        if (this.getDeviceID() === '2d001c000c47363433353735') {
          logger.info(this.getDeviceID(), 'KeepAlive');
        }
      
        this.updateAttributes({
          lastHeard: new Date()
        });
        this.sendReply('PingAck', packet.messageId);
        return;
      }

      if (!packet || packet.messageId !== this._receiveCounter) {
        logger.warn({
          deviceID: this.getDeviceID(),
          expect: this._receiveCounter,
          got: packet.messageId
        }, 'MessageId other than expected');

        if (requestType === 'Ignored') {
          // don't ignore an ignore...
          this.disconnect('Got an Ignore');
          return;
        } // this.sendMessage('Ignored', null, {}, null, null);


        this.disconnect('Bad Counter');
        return;
      }

      this.emit(requestType || '', packet);
    }
  }, {
    key: "sendReply",
    value: function sendReply(messageName, id, data, token, requester) {
      if (!this._isSocketAvailable(requester || null, messageName)) {
        return;
      } // if my reply is an acknowledgement to a confirmable message
      // then I need to re-use the message id...
      // set our counter


      if (id < 0) {
        this._incrementSendCounter();

        id = this._sendCounter; // eslint-disable-line no-param-reassign
      }

      var message = _CoapMessages["default"].wrap(messageName, id, null, null, data, token);

      if (!message) {
        logger.error({
          deviceID: this.getDeviceID()
        }, 'Device - could not unwrap message');
        return;
      }

      if (!this._cipherStream) {
        logger.error({
          deviceID: this.getDeviceID()
        }, 'Device - sendReply before READY');
        return;
      }

      this._cipherStream.write(message);
    }
  }, {
    key: "sendMessage",
    value: function sendMessage(messageName, params, options, data, requester) {
      var _this6 = this;

      if (!this._isSocketAvailable(requester, messageName)) {
        return -1;
      } // increment our counter


      this._incrementSendCounter();

      var token = null;

      if (!_CoapMessages["default"].isNonTypeMessage(messageName)) {
        this._incrementSendToken();

        this._useToken(messageName, this._sendToken);

        token = this._sendToken;
      }

      var message = _CoapMessages["default"].wrap(messageName, this._sendCounter, params, options, data, token);

      if (!message) {
        logger.error({
          data: data,
          deviceID: this.getDeviceID(),
          messageName: messageName,
          params: params
        }, 'Could not wrap message');
        return -1;
      }

      if (!this._cipherStream) {
        logger.error({
          deviceID: this.getDeviceID(),
          messageName: messageName
        }, 'Client - sendMessage before READY');
      }

      process.nextTick(function () {
        return !!_this6._cipherStream && _this6._cipherStream.write(message);
      });
      return token || 0;
    } // Adds a listener to our secure message stream

  }, {
    key: "listenFor",
    value: function () {
      var _listenFor = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(eventName, uri, token) {
        var _this7 = this;

        var tokenHex, beVerbose;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                tokenHex = token ? Device._toHexString(token) : null;
                beVerbose = _settings["default"].SHOW_VERBOSE_DEVICE_LOGS;
                return _context5.abrupt("return", new _promise["default"](function (resolve, reject) {
                  var timeout = (0, _setTimeout2["default"])(function () {
                    cleanUpListeners();
                    reject(new Error("Request timed out ".concat(eventName)));
                  }, KEEP_ALIVE_TIMEOUT); // adds a one time event

                  // adds a one time event
                  var handler = function handler(packet) {
                    var _packet$token;

                    clearTimeout(timeout);

                    var packetUri = _CoapMessages["default"].getUriPath(packet);

                    if (uri && (0, _indexOf["default"])(packetUri).call(packetUri, uri) !== 0) {
                      if (beVerbose) {
                        logger.warn({
                          deviceID: _this7.getDeviceID(),
                          packetUri: packetUri,
                          uri: uri
                        }, 'URI filter did not match');
                      }

                      return;
                    }

                    var packetTokenHex = (_packet$token = packet.token) === null || _packet$token === void 0 ? void 0 : _packet$token.toString('hex');

                    if (tokenHex && tokenHex !== packetTokenHex) {
                      if (beVerbose) {
                        logger.warn({
                          deviceID: _this7.getDeviceID(),
                          packetTokenHex: packetTokenHex,
                          tokenHex: tokenHex
                        }, 'Tokens did not match');
                      }

                      return;
                    }

                    cleanUpListeners();
                    resolve(packet);
                  };

                  var disconnectHandler = function disconnectHandler() {
                    cleanUpListeners();
                    reject();
                  };

                  var cleanUpListeners = function cleanUpListeners() {
                    _this7.removeListener(eventName, handler);

                    _this7.removeListener('disconnect', disconnectHandler);
                  };

                  _this7.on(eventName, handler);

                  _this7.on('disconnect', disconnectHandler);
                }));

              case 3:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function listenFor(_x2, _x3, _x4) {
        return _listenFor.apply(this, arguments);
      }

      return listenFor;
    }()
  }, {
    key: "_incrementSendCounter",
    value: function _incrementSendCounter() {
      this._sendCounter = Device._increment(this._sendCounter, COUNTER_MAX);
    }
  }, {
    key: "_incrementReceiveCounter",
    value: function _incrementReceiveCounter() {
      this._receiveCounter = Device._increment(this._receiveCounter, COUNTER_MAX);
    } // increments or wraps our token value, and makes sure it isn't in use

  }, {
    key: "_incrementSendToken",
    value: function _incrementSendToken() {
      this._sendToken = Device._increment(this._sendToken, TOKEN_COUNTER_MAX);

      this._clearToken(this._sendToken);

      return this._sendToken;
    }
    /**
     * Associates a particular token with a message we're sending, so we know
     * what we're getting back when we get an ACK
     */

  }, {
    key: "_useToken",
    value: function _useToken(name, sendToken) {
      var key = Device._toHexString(sendToken);

      if (this._tokens[key]) {
        var _context6, _context7;

        throw new Error((0, _concat["default"])(_context6 = (0, _concat["default"])(_context7 = "Token ".concat(name, " ")).call(_context7, this._tokens[key], " ")).call(_context6, key, " already in use"));
      }

      this._tokens[key] = name;
    } // clears the association with a particular token

  }, {
    key: "_clearToken",
    value: function _clearToken(sendToken) {
      var key = Device._toHexString(sendToken);

      if (this._tokens[key]) {
        delete this._tokens[key];
      }
    }
  }, {
    key: "_getResponseType",
    value: function _getResponseType(token) {
      var tokenString = token.toString('hex');
      var request = this._tokens[tokenString];

      if (!request) {
        return '';
      }

      return _CoapMessages["default"].getResponseType(request);
    }
  }, {
    key: "getVariableValue",
    value: function () {
      var _getVariableValue = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(name) {
        var isBusy, messageToken, message;
        return _regenerator["default"].wrap(function _callee6$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                isBusy = !this._isSocketAvailable(null);

                if (!isBusy) {
                  _context8.next = 3;
                  break;
                }

                throw new Error('This device is locked during the flashing process.');

              case 3:
                _context8.next = 5;
                return this.hasStatus(DEVICE_STATUS_MAP.READY);

              case 5:
                if (this._hasParticleVariable(name)) {
                  _context8.next = 7;
                  break;
                }

                throw new Error('Variable not found');

              case 7:
                messageToken = this.sendMessage('VariableRequest', {
                  name: name
                });
                _context8.next = 10;
                return this.listenFor('VariableValue', null, messageToken);

              case 10:
                message = _context8.sent;
                return _context8.abrupt("return", this._transformVariableResult(name, message));

              case 12:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee6, this);
      }));

      function getVariableValue(_x5) {
        return _getVariableValue.apply(this, arguments);
      }

      return getVariableValue;
    }()
  }, {
    key: "callFunction",
    value: function () {
      var _callFunction = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(functionName, functionArguments) {
        var isBusy, token, message;
        return _regenerator["default"].wrap(function _callee7$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                isBusy = !this._isSocketAvailable(null);

                if (!isBusy) {
                  _context9.next = 3;
                  break;
                }

                throw new Error('This device is locked during the flashing process.');

              case 3:
                _context9.next = 5;
                return this.hasStatus(DEVICE_STATUS_MAP.READY);

              case 5:
                if (this._hasSparkFunction(functionName)) {
                  _context9.next = 7;
                  break;
                }

                throw new Error('Function not found');

              case 7:
                logger.info({
                  deviceID: this.getDeviceID(),
                  functionName: functionName
                }, 'sending function call to the device');
                token = this.sendMessage('FunctionCall', {
                  args: (0, _values["default"])(functionArguments),
                  name: functionName
                });
                _context9.next = 11;
                return this.listenFor('FunctionReturn', null, token);

              case 11:
                message = _context9.sent;
                return _context9.abrupt("return", Device._transformFunctionResult(functionName, message));

              case 13:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee7, this);
      }));

      function callFunction(_x6, _x7) {
        return _callFunction.apply(this, arguments);
      }

      return callFunction;
    }()
    /**
     * Asks the device to start or stop its 'raise your hand' signal.
     * This will turn `nyan` mode on or off which just flashes the LED a bunch of
     * colors.
     */

  }, {
    key: "raiseYourHand",
    value: function () {
      var _raiseYourHand = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(shouldShowSignal) {
        var isBusy, buffer, token;
        return _regenerator["default"].wrap(function _callee8$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                isBusy = !this._isSocketAvailable(null);

                if (!isBusy) {
                  _context10.next = 3;
                  break;
                }

                throw new Error('This device is locked during the flashing process.');

              case 3:
                /**
                 * does the special URL writing needed directly to the COAP message object,
                 * since the URI requires non-text values
                 */
                buffer = Buffer.alloc(1);
                buffer.writeUInt8(shouldShowSignal ? 1 : 0, 0);
                token = this.sendMessage('SignalStart', null, [{
                  name: _CoapMessage["default"].Option.URI_QUERY,
                  value: buffer
                }]);
                return _context10.abrupt("return", this.listenFor('SignalStartReturn', null, token));

              case 7:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee8, this);
      }));

      function raiseYourHand(_x8) {
        return _raiseYourHand.apply(this, arguments);
      }

      return raiseYourHand;
    }()
  }, {
    key: "flash",
    value: function () {
      var _flash = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9(binary) {
        var fileTransferStore,
            address,
            isBusy,
            flasher,
            _args9 = arguments;
        return _regenerator["default"].wrap(function _callee9$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                fileTransferStore = _args9.length > 1 && _args9[1] !== undefined ? _args9[1] : _FileTransferStore["default"].FIRMWARE;
                address = _args9.length > 2 && _args9[2] !== undefined ? _args9[2] : '0x0';
                isBusy = !this._isSocketAvailable(null);

                if (!isBusy) {
                  _context11.next = 5;
                  break;
                }

                throw new Error('This device is locked during the flashing process.');

              case 5:
                this._isFlashing = true;
                flasher = new _Flasher["default"](this, this._maxBinarySize, this._otaChunkSize);
                _context11.prev = 7;
                logger.info({
                  deviceID: this.getDeviceID()
                }, 'flash device started! - sending api event');
                this.emit(DEVICE_EVENT_NAMES.FLASH_STARTED);
                _context11.next = 12;
                return flasher.startFlashBuffer(binary, fileTransferStore, address);

              case 12:
                logger.info({
                  deviceID: this.getDeviceID()
                }, 'Flash device finished! - sending api event');
                this.emit(DEVICE_EVENT_NAMES.FLASH_SUCCESS);
                this._isFlashing = false;
                this._owningFlasher = null;
                return _context11.abrupt("return", {
                  status: 'Update finished'
                });

              case 19:
                _context11.prev = 19;
                _context11.t0 = _context11["catch"](7);
                logger.error({
                  deviceID: this.getDeviceID(),
                  err: _context11.t0
                }, 'Flash device failed! - sending api event');
                this._isFlashing = false;
                this.emit(DEVICE_EVENT_NAMES.FLASH_FAILED);
                return _context11.abrupt("return", {
                  status: 'Update Failed'
                });

              case 25:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee9, this, [[7, 19]]);
      }));

      function flash(_x9) {
        return _flash.apply(this, arguments);
      }

      return flash;
    }()
  }, {
    key: "_isSocketAvailable",
    value: function _isSocketAvailable(requester, messageName) {
      if (!this._owningFlasher || this._owningFlasher === requester) {
        return true;
      }

      logger.error({
        cache_key: this._connectionKey,
        deviceID: this.getDeviceID(),
        messageName: messageName
      }, 'This client has an exclusive lock');
      return false;
    }
  }, {
    key: "takeOwnership",
    value: function takeOwnership(flasher) {
      if (this._owningFlasher) {
        logger.error({
          deviceID: this.getDeviceID()
        }, 'Device already owned');
        return false;
      } // only permit the owning object to send messages.


      this._owningFlasher = flasher;
      return true;
    }
  }, {
    key: "releaseOwnership",
    value: function releaseOwnership(flasher) {
      logger.info({
        deviceID: this.getDeviceID()
      }, 'Releasing flash ownership');

      if (this._owningFlasher === flasher) {
        this._owningFlasher = null;
      } else if (this._owningFlasher) {
        logger.error({
          deviceID: this.getDeviceID(),
          flasher: flasher
        }, "Cannot releaseOwnership, isn't  current owner");
      }
    }
  }, {
    key: "_transformVariableResult",
    value: function _transformVariableResult(name, packet) {
      // grab the variable type, if the device doesn't say, assume it's a 'string'
      var variableType = this._attributes.variables && this._attributes.variables[name] || 'string';
      var result = null;

      try {
        var _packet$payload;

        if ((_packet$payload = packet.payload) !== null && _packet$payload !== void 0 && _packet$payload.length) {
          // leaving raw payload in response message for now, so we don't shock
          // our users.
          result = _CoapMessages["default"].fromBinary(packet.payload, variableType);
        }
      } catch (error) {
        logger.error({
          err: error
        }, '_transformVariableResult - error transforming response');
      }

      return result;
    } // Transforms the result from a device function to the correct type.

  }, {
    key: "_getDescription",
    value: function () {
      var _getDescription2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee10() {
        var _this8 = this;

        return _regenerator["default"].wrap(function _callee10$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                _context12.next = 2;
                return new _promise["default"](function (resolve) {
                  (0, _setTimeout2["default"])(function () {
                    return resolve();
                  }, 50);
                });

              case 2:
                return _context12.abrupt("return", new _promise["default"](function (resolve, reject) {
                  var systemInformation;
                  var functionState;
                  var timeout = (0, _setTimeout2["default"])(function () {
                    cleanUpListeners();
                    reject(new Error('Request timed out - Describe'));
                  }, KEEP_ALIVE_TIMEOUT);

                  var handler = function handler(packet) {
                    var payload = packet.payload;

                    if (!(payload !== null && payload !== void 0 && payload.length)) {
                      throw new Error('Payload empty for Describe message');
                    }

                    var data = JSON.parse(payload.toString('utf8'));

                    if (!systemInformation && data.m) {
                      systemInformation = data;
                    }

                    if (data && data.v) {
                      functionState = data; // 'v':{'temperature':2}

                      // 'v':{'temperature':2}
                      (0, _nullthrows["default"])(functionState).v = _CoapMessages["default"].translateIntTypes((0, _nullthrows["default"])(data.v));
                    }

                    if (!systemInformation || !functionState) {
                      return;
                    }

                    clearTimeout(timeout);
                    cleanUpListeners();
                    resolve({
                      functionState: functionState,
                      systemInformation: systemInformation
                    });
                  };

                  var disconnectHandler = function disconnectHandler() {
                    cleanUpListeners();
                  };

                  var cleanUpListeners = function cleanUpListeners() {
                    _this8.removeListener('DescribeReturn', handler);

                    _this8.removeListener('disconnect', disconnectHandler);
                  };

                  _this8.on('DescribeReturn', handler);

                  _this8.on('disconnect', disconnectHandler); // Because some firmware versions do not send the app + system state
                  // in a single message, we cannot use `listenFor` and instead have to
                  // write some hacky code that duplicates a lot of the functionality


                  // Because some firmware versions do not send the app + system state
                  // in a single message, we cannot use `listenFor` and instead have to
                  // write some hacky code that duplicates a lot of the functionality
                  _this8.sendMessage('Describe');
                }));

              case 3:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee10);
      }));

      function _getDescription() {
        return _getDescription2.apply(this, arguments);
      }

      return _getDescription;
    }() //-------------
    // Device Events / Spark.publish / Spark.subscribe
    //-------------

  }, {
    key: "onDeviceEvent",
    value: function onDeviceEvent(event) {
      this.sendDeviceEvent(event);
    }
  }, {
    key: "sendDeviceEvent",
    value: function sendDeviceEvent(event) {
      var data = event.data,
          isPublic = event.isPublic,
          name = event.name,
          ttl = event.ttl;
      var messageName = isPublic ? DEVICE_MESSAGE_EVENTS_NAMES.PUBLIC_EVENT : DEVICE_MESSAGE_EVENTS_NAMES.PRIVATE_EVENT;
      this.sendMessage(messageName, {
        event_name: name
      }, [{
        name: _CoapMessage["default"].Option.MAX_AGE,
        value: _CoapMessages["default"].toBinary(ttl, 'uint32')
      }], data && Buffer.from(data) || null);
    }
  }, {
    key: "_hasParticleVariable",
    value: function _hasParticleVariable(name) {
      return !!(this._attributes.variables && this._attributes.variables[name]);
    }
  }, {
    key: "_hasSparkFunction",
    value: function _hasSparkFunction(functionName) {
      var _context13;

      return !!(this._attributes.functions && (0, _some["default"])(_context13 = this._attributes.functions).call(_context13, function (fn) {
        return fn.toLowerCase() === functionName.toLowerCase();
      }));
    }
  }, {
    key: "getDeviceID",
    value: function getDeviceID() {
      return this._attributes.deviceID;
    }
  }, {
    key: "getConnectionKey",
    value: function getConnectionKey() {
      return this._connectionKey;
    }
  }, {
    key: "getRemoteIPAddress",
    value: function getRemoteIPAddress() {
      return this._socket.remoteAddress ? this._socket.remoteAddress.toString() : 'unknown';
    }
  }, {
    key: "disconnect",
    value: function disconnect() {
      var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      this._disconnectCounter += 1;

      if (this._socketTimeoutInterval) {
        clearTimeout(this._socketTimeoutInterval);
        this._socketTimeoutInterval = null;
      }

      var logInfo = {
        cache_key: this._connectionKey,
        deviceID: this.getDeviceID(),
        duration: this._connectionStartTime ? (new Date() - (this._connectionStartTime || 0)) / 1000.0 : undefined
      };

      if (this._disconnectCounter > 1) {
        return;
      }

      try {
        logger.info(_objectSpread(_objectSpread({}, logInfo), {}, {
          disconnectCounter: this._disconnectCounter,
          message: message
        }), 'Device disconnected');
      } catch (error) {
        logger.error({
          deviceID: this.getDeviceID(),
          err: error
        }, 'Disconnect log error');
      }

      if (this._decipherStream) {
        try {
          this._decipherStream.end();

          this._decipherStream = null;
        } catch (error) {
          logger.error({
            deviceID: this.getDeviceID(),
            err: error
          }, 'Error cleaning up decipherStream');
        }
      }

      if (this._cipherStream) {
        try {
          this._cipherStream.end();

          this._cipherStream = null;
        } catch (error) {
          logger.error({
            deviceID: this.getDeviceID(),
            err: error
          }, 'Error cleaning up cipherStream');
        }
      }

      try {
        this._socket.end();

        this._socket.destroy();
      } catch (error) {
        logger.error({
          deviceID: this.getDeviceID(),
          err: error
        }, 'Disconnect TCPSocket error');
      }

      this.emit(DEVICE_EVENT_NAMES.DISCONNECT, message); // obv, don't do this before emitting disconnect.

      try {
        this.removeAllListeners();
      } catch (error) {
        logger.error({
          deviceID: this.getDeviceID(),
          err: error
        }, 'Problem removing listeners');
      }
    }
  }], [{
    key: "_increment",
    value: function _increment(counter, maxSize) {
      var resultCounter = counter + 1;
      return resultCounter < maxSize ? resultCounter : 0;
    }
  }, {
    key: "_transformFunctionResult",
    value: function _transformFunctionResult(name, packet) {
      var variableType = 'int32';
      var result = null;

      try {
        var _packet$payload2;

        if ((_packet$payload2 = packet.payload) !== null && _packet$payload2 !== void 0 && _packet$payload2.length) {
          result = _CoapMessages["default"].fromBinary(packet.payload, variableType);
        }
      } catch (error) {
        logger.error({
          err: error
        }, '_transformFunctionResult - error transforming response');
        throw error;
      }

      return result;
    }
  }, {
    key: "_toHexString",
    value: function _toHexString(value) {
      return Buffer.from([value]).toString('hex');
    }
  }]);
  return Device;
}(_events["default"]);

var _default = Device;
exports["default"] = _default;