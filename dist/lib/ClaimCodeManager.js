"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/map"));

var _setTimeout2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/set-timeout"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _crypto = _interopRequireDefault(require("crypto"));

var CLAIM_CODE_LENGTH = 63;
var CLAIM_CODE_TTL = 5000 * 60; // 5 min

var ClaimCodeManager = /*#__PURE__*/function () {
  function ClaimCodeManager() {
    (0, _classCallCheck2["default"])(this, ClaimCodeManager);
    (0, _defineProperty2["default"])(this, "_userIDByClaimCode", new _map["default"]());
  }

  (0, _createClass2["default"])(ClaimCodeManager, [{
    key: "_generateClaimCode",
    value: function _generateClaimCode() {
      return _crypto["default"].randomBytes(CLAIM_CODE_LENGTH).toString('base64').substring(0, CLAIM_CODE_LENGTH);
    }
  }, {
    key: "createClaimCode",
    value: function createClaimCode(userID) {
      var _this = this;

      var claimCode = this._generateClaimCode();

      while (this._userIDByClaimCode.has(claimCode)) {
        claimCode = this._generateClaimCode();
      }

      this._userIDByClaimCode.set(claimCode, userID);

      (0, _setTimeout2["default"])(function () {
        return _this.removeClaimCode(claimCode);
      }, CLAIM_CODE_TTL);
      return claimCode;
    }
  }, {
    key: "removeClaimCode",
    value: function removeClaimCode(claimCode) {
      return this._userIDByClaimCode["delete"](claimCode);
    }
  }, {
    key: "getUserIDByClaimCode",
    value: function getUserIDByClaimCode(claimCode) {
      return this._userIDByClaimCode.get(claimCode);
    }
  }]);
  return ClaimCodeManager;
}();

var _default = ClaimCodeManager;
exports["default"] = _default;