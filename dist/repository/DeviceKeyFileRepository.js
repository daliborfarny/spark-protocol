"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _getOwnPropertyDescriptor = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor"));

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _applyDecoratedDescriptor2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/applyDecoratedDescriptor"));

var _FileManager = _interopRequireDefault(require("./FileManager"));

var _memoizeGet = _interopRequireDefault(require("../decorators/memoizeGet"));

var _memoizeSet = _interopRequireDefault(require("../decorators/memoizeSet"));

var _dec, _dec2, _dec3, _dec4, _class;

var FILE_EXTENSION = '.pub.pem'; // getByID, deleteByID and update uses model.deviceID as ID for querying

var DeviceKeyFileRepository = (_dec = (0, _memoizeSet["default"])(), _dec2 = (0, _memoizeSet["default"])(['deviceID']), _dec3 = (0, _memoizeGet["default"])(['deviceID']), _dec4 = (0, _memoizeSet["default"])(), (_class = /*#__PURE__*/function () {
  function DeviceKeyFileRepository(path) {
    (0, _classCallCheck2["default"])(this, DeviceKeyFileRepository);
    (0, _defineProperty2["default"])(this, "_fileManager", void 0);
    this._fileManager = new _FileManager["default"](path);
  }

  (0, _createClass2["default"])(DeviceKeyFileRepository, [{
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(model) {
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this._fileManager.createFile(model.deviceID + FILE_EXTENSION, model.key);

                return _context.abrupt("return", model);

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function create(_x) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }, {
    key: "deleteByID",
    value: function () {
      var _deleteByID = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(deviceID) {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                this._fileManager.deleteFile(deviceID + FILE_EXTENSION);

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function deleteByID(_x2) {
        return _deleteByID.apply(this, arguments);
      }

      return deleteByID;
    }() // eslint-disable-next-line

  }, {
    key: "getAll",
    value: function () {
      var _getAll = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3() {
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                throw new Error('the method is not implemented');

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function getAll() {
        return _getAll.apply(this, arguments);
      }

      return getAll;
    }()
  }, {
    key: "getByID",
    value: function () {
      var _getByID = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(deviceID) {
        var key;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                key = this._fileManager.getFile(deviceID + FILE_EXTENSION);
                return _context4.abrupt("return", key ? {
                  algorithm: 'rsa',
                  deviceID: deviceID,
                  key: key
                } : null);

              case 2:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function getByID(_x3) {
        return _getByID.apply(this, arguments);
      }

      return getByID;
    }()
  }, {
    key: "updateByID",
    value: function () {
      var _updateByID = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(deviceID, props) {
        var key;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                key = props.key;

                this._fileManager.writeFile(deviceID + FILE_EXTENSION, key);

                return _context5.abrupt("return", {
                  algorithm: 'rsa',
                  deviceID: deviceID,
                  key: key
                });

              case 3:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function updateByID(_x4, _x5) {
        return _updateByID.apply(this, arguments);
      }

      return updateByID;
    }()
  }]);
  return DeviceKeyFileRepository;
}(), ((0, _applyDecoratedDescriptor2["default"])(_class.prototype, "create", [_dec], (0, _getOwnPropertyDescriptor["default"])(_class.prototype, "create"), _class.prototype), (0, _applyDecoratedDescriptor2["default"])(_class.prototype, "deleteByID", [_dec2], (0, _getOwnPropertyDescriptor["default"])(_class.prototype, "deleteByID"), _class.prototype), (0, _applyDecoratedDescriptor2["default"])(_class.prototype, "getByID", [_dec3], (0, _getOwnPropertyDescriptor["default"])(_class.prototype, "getByID"), _class.prototype), (0, _applyDecoratedDescriptor2["default"])(_class.prototype, "updateByID", [_dec4], (0, _getOwnPropertyDescriptor["default"])(_class.prototype, "updateByID"), _class.prototype)), _class));
var _default = DeviceKeyFileRepository;
exports["default"] = _default;