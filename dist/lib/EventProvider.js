"use strict";

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _EventPublisher = _interopRequireDefault(require("./EventPublisher"));

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context; _forEachInstanceProperty(_context = ownKeys(Object(source), true)).call(_context, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context2; _forEachInstanceProperty(_context2 = ownKeys(Object(source))).call(_context2, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

var EventProvider = /*#__PURE__*/function () {
  function EventProvider(eventPublisher) {
    (0, _classCallCheck2["default"])(this, EventProvider);
    (0, _defineProperty2["default"])(this, "_eventPublisher", void 0);
    this._eventPublisher = eventPublisher;
  }

  (0, _createClass2["default"])(EventProvider, [{
    key: "onNewEvent",
    value: function onNewEvent(callback) {
      var eventNamePrefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '*';

      this._eventPublisher.subscribe(eventNamePrefix, this._onNewEvent(callback), {
        filterOptions: {
          listenToBroadcastedEvents: false,
          listenToInternalEvents: false
        }
      });
    }
  }, {
    key: "_onNewEvent",
    value: function _onNewEvent(callback) {
      return function (event) {
        var eventToBroadcast = _objectSpread({}, event);

        callback(eventToBroadcast);
      };
    }
  }]);
  return EventProvider;
}();

var _default = EventProvider;
exports["default"] = _default;