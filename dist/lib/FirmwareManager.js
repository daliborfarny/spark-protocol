"use strict";

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _reduce = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/reduce"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _find = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/find"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _filter = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/filter"));

var _parseInt2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/parse-int"));

var _map2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/map"));

var _forEach = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/for-each"));

var _sort = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/sort"));

var _from = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/array/from"));

var _values = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/values"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/toConsumableArray"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/objectWithoutProperties"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _fs = _interopRequireDefault(require("fs"));

var _binaryVersionReader = require("binary-version-reader");

var _nullthrows = _interopRequireDefault(require("nullthrows"));

var _settings = _interopRequireDefault(require("../settings"));

var _settings2 = _interopRequireDefault(require("../../third-party/settings.json"));

var _logger = _interopRequireDefault(require("./logger"));

var _excluded = ["systemFiles"];

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty2(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context17; _forEachInstanceProperty2(_context17 = ownKeys(Object(source), true)).call(_context17, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context18; _forEachInstanceProperty2(_context18 = ownKeys(Object(source))).call(_context18, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

var logger = _logger["default"].createModuleLogger(module);

var NUMBER_BY_FUNCTION = {
  b: 2,
  s: 4,
  u: 5
};
var FUNC_BY_NUMBER = {
  2: 'b',
  4: 's',
  5: 'u'
};

var FirmwareManager = /*#__PURE__*/function () {
  function FirmwareManager() {
    (0, _classCallCheck2["default"])(this, FirmwareManager);
  }

  (0, _createClass2["default"])(FirmwareManager, [{
    key: "getKnownAppFileName",
    value: function getKnownAppFileName() {
      throw new Error('getKnownAppFileName has not been implemented.');
    }
  }], [{
    key: "getMissingModules",
    value: function getMissingModules(systemInformation) {
      return FirmwareManager._getMissingModule(systemInformation);
    }
  }, {
    key: "getOtaSystemUpdateConfig",
    value: function () {
      var _getOtaSystemUpdateConfig = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(systemInformation) {
        var missingDependencies, _missingDependencies$, systemFiles, result;

        return _regenerator["default"].wrap(function _callee$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                missingDependencies = FirmwareManager._getMissingModule(systemInformation);
                console.log('missingDependencies', 'missingDependencies');

                if (missingDependencies.length) {
                  _context6.next = 4;
                  break;
                }

                return _context6.abrupt("return", null);

              case 4:
                _missingDependencies$ = (0, _reduce["default"])(missingDependencies).call(missingDependencies, function (acc, dependency) {
                  var _context, _context2, _context3, _context4, _context5;

                  var dependencyPath = (0, _concat["default"])(_context = "".concat(_settings["default"].BINARIES_DIRECTORY, "/")).call(_context, dependency.filename);

                  if (!_fs["default"].existsSync(dependencyPath)) {
                    logger.error({
                      dependencyPath: dependencyPath,
                      dependency: dependency
                    }, 'Dependency does not exist on disk');
                    return acc;
                  }

                  var systemFile = _fs["default"].readFileSync(dependencyPath);

                  return {
                    allModuleFunctions: (0, _concat["default"])(_context2 = []).call(_context2, (0, _toConsumableArray2["default"])(acc.allModuleFunctions), [FUNC_BY_NUMBER[dependency.prefixInfo.moduleFunction]]),
                    allModuleIndices: (0, _concat["default"])(_context3 = []).call(_context3, (0, _toConsumableArray2["default"])(acc.allModuleIndices), [dependency.prefixInfo.moduleIndex]),
                    systemFiles: (0, _concat["default"])(_context4 = []).call(_context4, (0, _toConsumableArray2["default"])(acc.systemFiles), [systemFile]),
                    allUpdateFiles: (0, _concat["default"])(_context5 = []).call(_context5, (0, _toConsumableArray2["default"])(acc.allUpdateFiles), [dependency.filename])
                  };
                }, {
                  allModuleFunctions: [],
                  allModuleIndices: [],
                  systemFiles: [],
                  allUpdateFiles: []
                }), systemFiles = _missingDependencies$.systemFiles, result = (0, _objectWithoutProperties2["default"])(_missingDependencies$, _excluded);
                return _context6.abrupt("return", _objectSpread(_objectSpread({}, result), {}, {
                  systemFile: systemFiles[0]
                }));

              case 6:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee);
      }));

      function getOtaSystemUpdateConfig(_x) {
        return _getOtaSystemUpdateConfig.apply(this, arguments);
      }

      return getOtaSystemUpdateConfig;
    }()
  }, {
    key: "getAppModule",
    value: function getAppModule(systemInformation) {
      var _context7;

      var parser = new _binaryVersionReader.HalDescribeParser();
      return (0, _nullthrows["default"])((0, _find["default"])(_context7 = parser.getModules(systemInformation) // Filter so we only have the app modules
      ).call(_context7, function (module) {
        return module.func === 'u';
      }));
    }
  }, {
    key: "_getMissingModule",
    value: function _getMissingModule(systemInformation) {
      var _context8, _context9, _context12, _context14, _context15, _context16;

      var platformID = systemInformation.p; // Check CRC to see if there are any bad modules

      var parser = new _binaryVersionReader.HalDescribeParser();
      var knownMissingDependencies = (0, _map["default"])(_context8 = (0, _filter["default"])(_context9 = parser.getModules(systemInformation)).call(_context9, function (module) {
        return !module.hasIntegrity();
      })).call(_context8, function (module) {
        return module.toDescribe();
      });

      if (knownMissingDependencies.length) {
        logger.info('Bad CRC for firmware');
      } else {
        // find missing dependencies
        var dr = new _binaryVersionReader.HalDependencyResolver();
        knownMissingDependencies = dr.findAnyMissingDependencies(systemInformation);
      }

      if (!knownMissingDependencies.length) {
        return [];
      }

      var findSettingForFirmwareModule = function findSettingForFirmwareModule(dependency) {
        return (0, _find["default"])(_settings2["default"]).call(_settings2["default"], function (_ref) {
          var prefixInfo = _ref.prefixInfo;
          return prefixInfo.platformID === platformID && prefixInfo.moduleVersion === dependency.version && prefixInfo.moduleFunction === NUMBER_BY_FUNCTION[dependency.func] && prefixInfo.moduleIndex === (0, _parseInt2["default"])(dependency.name, 10);
        });
      };

      var addRealDependencies = function addRealDependencies(dependency) {
        var _context10;

        var setting = findSettingForFirmwareModule(dependency);

        if (!setting) {
          logger.error('Cannot find firmware for module', {
            systemInformation: systemInformation,
            dependency: dependency
          });
          return null;
        }

        var result = new _binaryVersionReader.FirmwareModule(_objectSpread(_objectSpread({}, dependency.toDescribe()), {}, {
          d: (0, _filter["default"])(_context10 = [{
            f: FUNC_BY_NUMBER[setting.prefixInfo.depModuleFunction],
            n: setting.prefixInfo.depModuleIndex.toString(),
            v: setting.prefixInfo.depModuleVersion
          }, {
            f: FUNC_BY_NUMBER[setting.prefixInfo.dep2ModuleFunction],
            n: setting.prefixInfo.dep2ModuleIndex.toString(),
            v: setting.prefixInfo.dep2ModuleVersion
          }]).call(_context10, function (_ref2) {
            var v = _ref2.v;
            return v !== 0;
          })
        }));
        result.filename = setting.filename;
        return result;
      };

      var allMissingDependencies = new _map2["default"]();

      var setDependency = function setDependency(dep) {
        var _context11;

        var key = (0, _concat["default"])(_context11 = "".concat(dep.func, "#")).call(_context11, dep.name);

        if (allMissingDependencies.has(key)) {
          var currentDep = allMissingDependencies.get(key);

          if (currentDep.version > dep.version) {
            return;
          }
        }

        allMissingDependencies.set(key, dep);
      };

      var results = (0, _filter["default"])(_context12 = (0, _map["default"])(knownMissingDependencies).call(knownMissingDependencies, function (dep) {
        return addRealDependencies(new _binaryVersionReader.FirmwareModule(dep));
      })).call(_context12, Boolean);
      (0, _forEach["default"])(results).call(results, setDependency);

      while (results.length) {
        var result = results.pop();
        var unmetDependencies = [];

        if (!result.areDependenciesMet(systemInformation.m, unmetDependencies)) {
          var _context13;

          var settingsModules = (0, _filter["default"])(_context13 = (0, _map["default"])(unmetDependencies).call(unmetDependencies, addRealDependencies)).call(_context13, Boolean);
          results.push.apply(results, (0, _toConsumableArray2["default"])(settingsModules));
          (0, _forEach["default"])(settingsModules).call(settingsModules, setDependency);
        }
      } // Map dependencies to firmware metadata


      var knownMissingFirmwares = (0, _filter["default"])(_context14 = (0, _map["default"])(_context15 = (0, _sort["default"])(_context16 = (0, _from["default"])((0, _values["default"])(allMissingDependencies).call(allMissingDependencies))).call(_context16, function (a, b) {
        if (a.func === 'b') {
          return 1;
        }

        return a.name - b.name;
      })).call(_context15, function (dep) {
        var setting = findSettingForFirmwareModule(dep);

        if (!setting) {
          logger.error({
            dep: dep,
            platformID: platformID
          }, 'Missing firmware setting');
        }

        return setting;
      })).call(_context14, Boolean);
      logger.info({
        allFileNames: (0, _map["default"])(knownMissingFirmwares).call(knownMissingFirmwares, function (firmware) {
          return firmware.filename;
        })
      }, 'Discovering missing firmware');
      return knownMissingFirmwares;
    }
  }]);
  return FirmwareManager;
}();

var _default = FirmwareManager;
exports["default"] = _default;