// flow-typed signature: bc166dea5bd487f182600372bb4e972f
// flow-typed version: <<STUB>>/ec-key_v0.0.4/flow_v0.165.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'ec-key'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'ec-key' {
  declare class ECKey {
    constructor(any, any, any): ECKey;
    createSign(string): ECKey;
    update(Buffer): ECKey;
    sign(): Buffer;
    toString(string): string;
  }
  declare export default typeof ECKey;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'ec-key/gulpfile' {
  declare module.exports: any;
}

declare module 'ec-key/src/ec-key' {
  declare module.exports: any;
}

declare module 'ec-key/test/ec-key.test' {
  declare module.exports: any;
}

// Filename aliases
declare module 'ec-key/gulpfile.js' {
  declare module.exports: $Exports<'ec-key/gulpfile'>;
}
declare module 'ec-key/src/ec-key.js' {
  declare module.exports: $Exports<'ec-key/src/ec-key'>;
}
declare module 'ec-key/test/ec-key.test.js' {
  declare module.exports: $Exports<'ec-key/test/ec-key.test'>;
}
