"use strict";

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _DeviceAttributeFileRepository = _interopRequireDefault(require("./repository/DeviceAttributeFileRepository"));

var _DeviceKeyFileRepository = _interopRequireDefault(require("./repository/DeviceKeyFileRepository"));

var _DeviceServer = _interopRequireDefault(require("./server/DeviceServer"));

var _EventPublisher = _interopRequireDefault(require("./lib/EventPublisher"));

var _EventProvider = _interopRequireDefault(require("./lib/EventProvider"));

var _ClaimCodeManager = _interopRequireDefault(require("./lib/ClaimCodeManager"));

var _CryptoManager = _interopRequireDefault(require("./lib/CryptoManager"));

var _MockProductDeviceRepository = _interopRequireDefault(require("./repository/MockProductDeviceRepository"));

var _MockProductFirmwareRepository = _interopRequireDefault(require("./repository/MockProductFirmwareRepository"));

var _ServerKeyFileRepository = _interopRequireDefault(require("./repository/ServerKeyFileRepository"));

var _settings = _interopRequireDefault(require("./settings"));

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context; _forEachInstanceProperty(_context = ownKeys(Object(source), true)).call(_context, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context2; _forEachInstanceProperty(_context2 = ownKeys(Object(source))).call(_context2, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

var defaultBindings = function defaultBindings(container, serverSettings) {
  var mergedSettings = _objectSpread(_objectSpread({}, _settings["default"]), serverSettings); // Settings


  container.bindValue('DEVICE_DIRECTORY', mergedSettings.DEVICE_DIRECTORY);
  container.bindValue('CONNECTED_DEVICES_LOGGING_INTERVAL', mergedSettings.CONNECTED_DEVICES_LOGGING_INTERVAL);
  container.bindValue('ENABLE_SYSTEM_FIRWMARE_AUTOUPDATES', mergedSettings.ENABLE_SYSTEM_FIRWMARE_AUTOUPDATES);
  container.bindValue('SERVER_KEY_FILENAME', mergedSettings.SERVER_KEY_FILENAME);
  container.bindValue('SERVER_KEY_PASSWORD', mergedSettings.SERVER_KEY_PASSWORD);
  container.bindValue('SERVER_KEYS_DIRECTORY', mergedSettings.SERVER_KEYS_DIRECTORY);
  container.bindValue('TCP_DEVICE_SERVER_CONFIG', mergedSettings.TCP_DEVICE_SERVER_CONFIG);
  container.bindValue('ALLOW_DEVICE_TO_PROVIDE_PEM', mergedSettings.ALLOW_DEVICE_TO_PROVIDE_PEM); // Repository

  container.bindClass('IDeviceAttributeRepository', _DeviceAttributeFileRepository["default"], ['DEVICE_DIRECTORY']);
  container.bindClass('IDeviceKeyRepository', _DeviceKeyFileRepository["default"], ['DEVICE_DIRECTORY']);
  container.bindClass('IProductDeviceRepository', _MockProductDeviceRepository["default"]);
  container.bindClass('IProductFirmwareRepository', _MockProductFirmwareRepository["default"]);
  container.bindClass('ServerKeyRepository', _ServerKeyFileRepository["default"], ['SERVER_KEYS_DIRECTORY', 'SERVER_KEY_FILENAME']); // Utils

  container.bindClass('EventPublisher', _EventPublisher["default"], []);
  container.bindClass('EVENT_PROVIDER', _EventProvider["default"], ['EventPublisher']);
  container.bindClass('ClaimCodeManager', _ClaimCodeManager["default"], []);
  container.bindClass('CryptoManager', _CryptoManager["default"], ['IDeviceKeyRepository', 'ServerKeyRepository', 'SERVER_KEY_PASSWORD']); // Device server

  container.bindClass('DeviceServer', _DeviceServer["default"], ['IDeviceAttributeRepository', 'IProductDeviceRepository', 'IProductFirmwareRepository', 'ClaimCodeManager', 'CryptoManager', 'EventPublisher', 'TCP_DEVICE_SERVER_CONFIG', 'ENABLE_SYSTEM_FIRWMARE_AUTOUPDATES', 'CONNECTED_DEVICES_LOGGING_INTERVAL', 'ALLOW_DEVICE_TO_PROVIDE_PEM']);
};

var _default = defaultBindings;
exports["default"] = _default;