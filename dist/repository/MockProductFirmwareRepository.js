"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

/* eslint-disable no-unused-vars */
// getByID, deleteByID and update uses model.deviceID as ID for querying
var MockProductFirmwareRepository = /*#__PURE__*/function () {
  function MockProductFirmwareRepository() {
    (0, _classCallCheck2["default"])(this, MockProductFirmwareRepository);
  }

  (0, _createClass2["default"])(MockProductFirmwareRepository, [{
    key: "create",
    value: function create(model) {
      throw new Error('The method is not implemented');
    }
  }, {
    key: "deleteByID",
    value: function deleteByID(productFirmwareID) {
      throw new Error('The method is not implemented');
    } // eslint-disable-next-line

  }, {
    key: "getAll",
    value: function getAll() {
      throw new Error('The method is not implemented');
    }
  }, {
    key: "getAllByProductID",
    value: function getAllByProductID(productID) {
      throw new Error('The method is not implemented');
    }
  }, {
    key: "getByVersionForProduct",
    value: function getByVersionForProduct(productID, version) {
      throw new Error('The method is not implemented');
    }
  }, {
    key: "getCurrentForProduct",
    value: function getCurrentForProduct(productID) {
      throw new Error('The method is not implemented');
    }
  }, {
    key: "getByID",
    value: function getByID(productFirmwareID) {
      throw new Error('The method is not implemented');
    }
  }, {
    key: "updateByID",
    value: function updateByID(productFirmwareID, props) {
      throw new Error('The method is not implemented');
    }
  }]);
  return MockProductFirmwareRepository;
}();

var _default = MockProductFirmwareRepository;
exports["default"] = _default;