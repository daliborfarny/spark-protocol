/*
 *   Copyright (c) 2015 Particle Industries, Inc.  All rights reserved.
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * @flow
 *
 */

import path from 'path';

export type Settings = {
  BINARIES_DIRECTORY: string,
  DEFAULT_EVENT_TTL: number,
  DEVICE_DIRECTORY: string,
  TCP_DEVICE_SERVER_CONFIG: {
    ENABLE_SYSTEM_FIRWMARE_AUTOUPDATES: boolean,
    HOST: string,
    PORT: number,
  },
  SERVER_KEY_FILENAME: string,
  SERVER_KEY_PASSWORD: ?string,
  SERVER_KEYS_DIRECTORY: string,

  CRYPTO_ALGORITHM: string,
  LOG_LEVEL: string,
  KEEP_ALIVE_TIMEOUT: number,
  SOCKET_TIMEOUT: number,

  VERBOSE_PROTOCOL: boolean,
  SHOW_VERBOSE_DEVICE_LOGS: boolean,
  CONNECTED_DEVICES_LOGGING_INTERVAL: number,
  // Sometimes the device keys become corrupted (or you might have forgotten to update the key)
  // This will save the key for the device instead of using CLI to set it.
  ALLOW_DEVICE_TO_PROVIDE_PEM: boolean,
};

const SETTINGS: Settings = {
  BINARIES_DIRECTORY: path.join(process.cwd(), 'data/binaries'),
  DEFAULT_EVENT_TTL: 60,
  DEVICE_DIRECTORY: path.join(process.cwd(), 'data/deviceKeys'),
  TCP_DEVICE_SERVER_CONFIG: {
    ENABLE_SYSTEM_FIRWMARE_AUTOUPDATES: true,
    HOST: 'localhost',
    PORT: 5683,
  },
  SERVER_KEY_FILENAME: 'default_key.pem',
  SERVER_KEY_PASSWORD: (null: ?string),
  SERVER_KEYS_DIRECTORY: path.join(__dirname, '../data/users'),

  CRYPTO_ALGORITHM: 'aes-128-cbc',
  LOG_LEVEL: (process.env.LOG_LEVEL: any) || 'info',
  KEEP_ALIVE_TIMEOUT: 15000, // 15 seconds
  SOCKET_TIMEOUT: 101000, // 31 seconds

  VERBOSE_PROTOCOL: false,
  SHOW_VERBOSE_DEVICE_LOGS: false,
  CONNECTED_DEVICES_LOGGING_INTERVAL: 10000,
  ALLOW_DEVICE_TO_PROVIDE_PEM: false,
};

export default SETTINGS;
