"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _ecKey = _interopRequireDefault(require("ec-key"));

var _nodeRsa = _interopRequireDefault(require("node-rsa"));

var DeviceKey = /*#__PURE__*/function () {
  function DeviceKey(pemString) {
    (0, _classCallCheck2["default"])(this, DeviceKey);
    (0, _defineProperty2["default"])(this, "_ecKey", void 0);
    (0, _defineProperty2["default"])(this, "_nodeRsa", void 0);

    try {
      this._nodeRsa = new _nodeRsa["default"](pemString, 'pkcs8-public-pem', {
        encryptionScheme: 'pkcs1',
        signingScheme: 'pkcs1'
      });
    } catch (_) {
      this._ecKey = new _ecKey["default"](pemString, 'pem');
    }
  }

  (0, _createClass2["default"])(DeviceKey, [{
    key: "encrypt",
    value: function encrypt(data) {
      if (this._nodeRsa) {
        return this._nodeRsa.encrypt(data);
      }

      if (this._ecKey) {
        return this._ecKey.createSign('SHA256').update(data).sign();
      }

      throw new Error("Key not implemented ".concat(data.toString()));
    }
  }, {
    key: "equals",
    value: function equals(publicKeyPem) {
      if (!publicKeyPem) {
        return false;
      }

      var otherKey = new DeviceKey(publicKeyPem);
      return this.toPem() === otherKey.toPem();
    }
  }, {
    key: "toPem",
    value: function toPem() {
      if (this._nodeRsa) {
        return this._nodeRsa.exportKey('pkcs8-public-pem').toString();
      }

      if (this._ecKey) {
        return this._ecKey.toString('pem');
      }

      return null;
    }
  }]);
  return DeviceKey;
}();

var _default = DeviceKey;
exports["default"] = _default;