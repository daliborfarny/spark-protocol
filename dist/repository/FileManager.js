"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _filter = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/filter"));

var _endsWith = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/ends-with"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _fs = _interopRequireDefault(require("fs"));

var _mkdirp = _interopRequireDefault(require("mkdirp"));

var _path = _interopRequireDefault(require("path"));

var FileManager = /*#__PURE__*/function () {
  function FileManager(directoryPath) {
    var isJSON = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
    (0, _classCallCheck2["default"])(this, FileManager);
    (0, _defineProperty2["default"])(this, "_directoryPath", void 0);
    (0, _defineProperty2["default"])(this, "_isJSON", void 0);
    this._directoryPath = directoryPath;
    this._isJSON = isJSON;

    if (!_fs["default"].existsSync(directoryPath)) {
      _mkdirp["default"].sync(directoryPath);
    }
  }

  (0, _createClass2["default"])(FileManager, [{
    key: "count",
    value: function count() {
      return _fs["default"].readdirSync(this._directoryPath).length;
    }
  }, {
    key: "createFile",
    value: function createFile(fileName, data) {
      if (_fs["default"].existsSync(_path["default"].join(this._directoryPath, fileName))) {
        return;
      }

      this.writeFile(fileName, data);
    }
  }, {
    key: "deleteFile",
    value: function deleteFile(fileName) {
      var filePath = _path["default"].join(this._directoryPath, fileName);

      if (!_fs["default"].existsSync(filePath)) {
        return;
      }

      _fs["default"].unlinkSync(filePath);
    }
  }, {
    key: "getAllData",
    value: function getAllData() {
      var _context,
          _context2,
          _this = this;

      return (0, _map["default"])(_context = (0, _filter["default"])(_context2 = _fs["default"].readdirSync(this._directoryPath)).call(_context2, function (fileName) {
        return (0, _endsWith["default"])(fileName).call(fileName, '.json');
      })).call(_context, function (fileName) {
        return _fs["default"].readFileSync(_path["default"].join(_this._directoryPath, fileName), 'utf8');
      });
    }
  }, {
    key: "getFile",
    value: function getFile(fileName) {
      var filePath = _path["default"].join(this._directoryPath, fileName);

      if (!_fs["default"].existsSync(filePath)) {
        return null;
      }

      return _fs["default"].readFileSync(filePath, 'utf8');
    }
  }, {
    key: "getFileBuffer",
    value: function getFileBuffer(fileName) {
      var filePath = _path["default"].join(this._directoryPath, fileName);

      if (!_fs["default"].existsSync(filePath)) {
        return null;
      }

      return _fs["default"].readFileSync(filePath);
    }
  }, {
    key: "hasFile",
    value: function hasFile(fileName) {
      var filePath = _path["default"].join(this._directoryPath, fileName);

      return _fs["default"].existsSync(filePath);
    }
  }, {
    key: "writeFile",
    value: function writeFile(fileName, data) {
      _fs["default"].writeFileSync(_path["default"].join(this._directoryPath, fileName), data);
    }
  }]);
  return FileManager;
}();

var _default = FileManager;
exports["default"] = _default;