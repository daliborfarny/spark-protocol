"use strict";

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/toConsumableArray"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _filter = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/filter"));

var _every = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/every"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _forEach = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/for-each"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _indexOf = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/index-of"));

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty2(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context7; _forEachInstanceProperty2(_context7 = ownKeys(Object(source), true)).call(_context7, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context8; _forEachInstanceProperty2(_context8 = ownKeys(Object(source))).call(_context8, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable no-param-reassign */

/* eslint-disable func-names */
var _default = function _default() {
  var parameterKeys = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  return function (target, name, descriptor) {
    var descriptorFunction = descriptor.value;

    var fetchItemFunction = function fetchItemFunction(item) {
      return item;
    };

    if (!parameterKeys) {
      fetchItemFunction = function fetchItemFunction(item) {
        return item;
      };
    } else if (parameterKeys[0] === 'id') {
      fetchItemFunction = function fetchItemFunction(id) {
        return this.getByID(id);
      };
    } else {
      fetchItemFunction = function fetchItemFunction(keys) {
        return function () {
          var _context;

          for (var _len = arguments.length, parameters = new Array(_len), _key = 0; _key < _len; _key++) {
            parameters[_key] = arguments[_key];
          }

          return (0, _filter["default"])(_context = this.getAll()).call(_context, function (item) {
            return (0, _every["default"])(parameters).call(parameters, function (param, index) {
              return param === item[keys[index]];
            });
          });
        };
      };
    }

    descriptor.value = /*#__PURE__*/(0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
      var _context2, _fetchItemFunction, _context3, _context4;

      var args,
          result,
          item,
          _args = arguments;
      return _regenerator["default"].wrap(function _callee$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              args = _args; // eslint-disable-line prefer-rest-params

              _context6.next = 3;
              return descriptorFunction.call.apply(descriptorFunction, (0, _concat["default"])(_context2 = [this]).call(_context2, (0, _toConsumableArray2["default"])(args)));

            case 3:
              result = _context6.sent;
              _context6.t0 = _objectSpread;
              _context6.t1 = _objectSpread({}, result);
              _context6.next = 8;
              return (_fetchItemFunction = fetchItemFunction).call.apply(_fetchItemFunction, (0, _concat["default"])(_context3 = [this]).call(_context3, (0, _toConsumableArray2["default"])(args)));

            case 8:
              _context6.t2 = _context6.sent;
              item = (0, _context6.t0)(_context6.t1, _context6.t2);
              (0, _forEach["default"])(_context4 = target._caches).call(_context4, function (cache) {
                var _context5;

                (0, _forEach["default"])(_context5 = cache.keySets).call(_context5, function (keySet) {
                  var _cache$memoized;

                  if (!keySet || !keySet.length) {
                    cache.memoized.clear();
                    return;
                  } // Either get the parameter out of item or the args.


                  var cacheParams = (0, _map["default"])(keySet).call(keySet, function (key) {
                    return item[key] || args[(0, _indexOf["default"])(keySet).call(keySet, key)];
                  });

                  (_cache$memoized = cache.memoized)["delete"].apply(_cache$memoized, (0, _toConsumableArray2["default"])(cacheParams));
                });
              });
              return _context6.abrupt("return", result);

            case 12:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee, this);
    }));
    return descriptor;
  };
};

exports["default"] = _default;