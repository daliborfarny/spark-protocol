"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _slice = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/slice"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

/*
 *   Copyright (c) 2015 Particle Industries, Inc.  All rights reserved.
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation, either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * 
 *
 */
var BufferStream = /*#__PURE__*/function () {
  function BufferStream(buffer) {
    (0, _classCallCheck2["default"])(this, BufferStream);
    (0, _defineProperty2["default"])(this, "_buffer", void 0);
    (0, _defineProperty2["default"])(this, "_index", 0);
    this._buffer = buffer;
  }

  (0, _createClass2["default"])(BufferStream, [{
    key: "seek",
    value: function seek(index) {
      this._index = index;
    }
  }, {
    key: "read",
    value: function read(size) {
      if (!this._buffer) {
        return null;
      }

      var index = this._index;
      var endIndex = index + (size || 0);

      if (endIndex >= this._buffer.length) {
        endIndex = this._buffer.length;
      }

      var result = null;

      if (endIndex - index > 0) {
        var _context;

        result = (0, _slice["default"])(_context = this._buffer).call(_context, index, endIndex);
        this._index = endIndex;
      }

      return result;
    }
  }, {
    key: "close",
    value: function close() {
      this._buffer = null;
    }
  }]);
  return BufferStream;
}();

var _default = BufferStream;
exports["default"] = _default;