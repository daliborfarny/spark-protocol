"use strict";

var _Reflect$construct = require("@babel/runtime-corejs3/core-js-stable/reflect/construct");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _parseInt2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/parse-int"));

var _slice = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/slice"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/getPrototypeOf"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _stream = require("stream");

var _logger = _interopRequireDefault(require("./logger"));

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = _Reflect$construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !_Reflect$construct) return false; if (_Reflect$construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(_Reflect$construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var logger = _logger["default"].createModuleLogger(module);
/**
 Our job here is to accept messages in whole chunks, and put their length in front
 as we send them out, and parse them back into those size chunks as we read them in.
 * */

/* eslint-disable no-bitwise */


var MSG_LENGTH_BYTES = 2;

var messageLengthBytes = function messageLengthBytes(message) {
  // assuming a maximum encrypted message length of 65K, lets write an
  // unsigned short int before every message, so we know how much to read out.
  var length = message.length;
  var lengthBuffer = Buffer.alloc(MSG_LENGTH_BYTES);
  lengthBuffer[0] = length >>> 8;
  lengthBuffer[1] = length & 255;
  return lengthBuffer;
};

var ChunkingStream = /*#__PURE__*/function (_Transform) {
  (0, _inherits2["default"])(ChunkingStream, _Transform);

  var _super = _createSuper(ChunkingStream);

  function ChunkingStream(options) {
    var _this;

    (0, _classCallCheck2["default"])(this, ChunkingStream);
    _this = _super.call(this);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_combinedBuffer", null);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_currentOffset", 0);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_options", void 0);
    _this._options = options;
    return _this;
  }

  (0, _createClass2["default"])(ChunkingStream, [{
    key: "_transform",
    value: function _transform(buffer, encoding, callback) {
      if (this._options.outgoing) {
        this._processOutput(buffer, encoding, callback);
      } else {
        this._processInput(buffer, encoding, callback);
      }
    }
  }, {
    key: "_processOutput",
    value: function _processOutput(buffer, encoding, callback) {
      var tempBuffer = typeof buffer === 'string' ? Buffer.from(buffer) : buffer;
      var lengthChunk = messageLengthBytes(tempBuffer);
      this.push((0, _concat["default"])(Buffer).call(Buffer, lengthChunk ? [lengthChunk, tempBuffer] : [tempBuffer]));
      process.nextTick(callback);
    }
  }, {
    key: "_processInput",
    value: function _processInput(buffer, encoding, callback) {
      var _this2 = this;

      try {
        var copyStart = 0;
        var tempBuffer = typeof buffer === 'string' ? Buffer.from(buffer) : buffer;

        if (this._combinedBuffer === null) {
          var expectedLength = (0, _parseInt2["default"])((tempBuffer[0] << 8) + buffer[1], 10);
          this._combinedBuffer = Buffer.alloc(expectedLength);
          this._currentOffset = 0;
          copyStart = 2;
        }

        var combinedBuffer = this._combinedBuffer;

        if (combinedBuffer == null) {
          return;
        }

        var copyEnd = Math.min(buffer.length, combinedBuffer.length - this._currentOffset + copyStart);
        this._currentOffset += tempBuffer.copy(combinedBuffer, this._currentOffset, copyStart, copyEnd);

        if (this._currentOffset !== combinedBuffer.length) {
          process.nextTick(callback);
          return;
        }

        this.push(combinedBuffer);
        this._combinedBuffer = null;

        if (tempBuffer.length <= copyEnd) {
          process.nextTick(callback);
          return;
        }

        var remainder = (0, _slice["default"])(buffer).call(buffer, copyEnd);
        process.nextTick(function () {
          return _this2._processInput(remainder, encoding, callback);
        });
      } catch (error) {
        logger.error({
          err: error
        }, 'ChunkingStream error!');
        process.nextTick(callback);
      }
    }
  }]);
  return ChunkingStream;
}(_stream.Transform);

var _default = ChunkingStream;
exports["default"] = _default;