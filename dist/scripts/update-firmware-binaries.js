#! /usr/bin/env node
"use strict";

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _slice = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/slice"));

var _filter = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/filter"));

var _reduce = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/reduce"));

var _promise = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/promise"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _stringify = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/json/stringify"));

var _sort = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/sort"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/toConsumableArray"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _rest = require("@octokit/rest");

var _mkdirp = _interopRequireDefault(require("mkdirp"));

var _binaryVersionReader = require("binary-version-reader");

var _dotenv = _interopRequireDefault(require("dotenv"));

var _settings = _interopRequireDefault(require("../settings"));

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty2(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context15; _forEachInstanceProperty(_context15 = ownKeys(Object(source), true)).call(_context15, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context16; _forEachInstanceProperty(_context16 = ownKeys(Object(source))).call(_context16, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

var fileDirectory = _path["default"].resolve(process.env.INIT_CWD);

var filePath = null; // A counter is a lot safer than a while(true)

var count = 0;

while (count < 20) {
  count += 1;
  filePath = _path["default"].join(fileDirectory, '.env');
  console.log('Checking for .env: ', filePath);

  if (_fs["default"].existsSync(filePath)) {
    break;
  }

  var newFileDirectory = _path["default"].join(fileDirectory, '..');

  if (newFileDirectory === fileDirectory) {
    filePath = null;
    break;
  }

  fileDirectory = newFileDirectory;
}

if (!filePath) {
  _dotenv["default"].config();
} else {
  _dotenv["default"].config({
    path: filePath
  });
}

var GITHUB_USER = 'particle-iot';
var GITHUB_FIRMWARE_REPOSITORY = 'firmware';
var GITHUB_CLI_REPOSITORY = 'particle-cli';

var FILE_GEN_DIRECTORY = _path["default"].join(__dirname, '../../third-party/');

var SETTINGS_FILE = "".concat(FILE_GEN_DIRECTORY, "settings.json");
var GITHUB_AUTH_TOKEN = process.env.GITHUB_AUTH_TOKEN;

if (!GITHUB_AUTH_TOKEN) {
  throw new Error('You need to set up a .env file with auth credentials (read public repos)');
}

var githubAPI = new _rest.Octokit({
  auth: GITHUB_AUTH_TOKEN
});

var downloadAssetFile = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(asset) {
    var _context;

    var filename, fileWithPath;
    return _regenerator["default"].wrap(function _callee$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            filename = asset.name;
            fileWithPath = (0, _concat["default"])(_context = "".concat(_settings["default"].BINARIES_DIRECTORY, "/")).call(_context, filename);

            if (!_fs["default"].existsSync(fileWithPath)) {
              _context3.next = 5;
              break;
            }

            console.log("File Exists: ".concat(filename));
            return _context3.abrupt("return", filename);

          case 5:
            console.log("Downloading ".concat(filename, "..."));
            return _context3.abrupt("return", githubAPI.rest.repos.getReleaseAsset({
              headers: {
                accept: 'application/octet-stream'
              },
              asset_id: asset.id,
              owner: GITHUB_USER,
              repo: GITHUB_FIRMWARE_REPOSITORY
            }).then(function (response) {
              _fs["default"].writeFileSync(fileWithPath, Buffer.from(response.data));

              return filename;
            })["catch"](function (error) {
              var _context2;

              return console.error((0, _concat["default"])(_context2 = "Could not download ".concat(asset.name, " -- ")).call(_context2, error.message));
            }));

          case 7:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee);
  }));

  return function downloadAssetFile(_x) {
    return _ref.apply(this, arguments);
  };
}();

var downloadBlob = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(asset) {
    var _context4;

    var filename, fileWithPath;
    return _regenerator["default"].wrap(function _callee2$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            filename = asset.name;
            fileWithPath = (0, _concat["default"])(_context4 = "".concat(_settings["default"].BINARIES_DIRECTORY, "/")).call(_context4, filename);

            if (!_fs["default"].existsSync(fileWithPath)) {
              _context5.next = 5;
              break;
            }

            console.log("File Exists: ".concat(filename));
            return _context5.abrupt("return", filename);

          case 5:
            console.log("Downloading ".concat(filename, "..."));
            return _context5.abrupt("return", githubAPI.rest.git.getBlob({
              file_sha: asset.sha,
              headers: {
                accept: 'application/vnd.github.v3.raw'
              },
              owner: GITHUB_USER,
              repo: GITHUB_CLI_REPOSITORY
            }).then(function (response) {
              _fs["default"].writeFileSync(fileWithPath, Buffer.from(response.data));

              return filename;
            })["catch"](function (error) {
              return console.error(error);
            }));

          case 7:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee2);
  }));

  return function downloadBlob(_x2) {
    return _ref2.apply(this, arguments);
  };
}();

var downloadFirmwareBinaries = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(assets) {
    var CHUNK_SIZE, chunkArray, chunks, assetFileNames;
    return _regenerator["default"].wrap(function _callee3$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            chunkArray = function _chunkArray(input, chunkSize) {
              var index = 0;
              var arrayLength = input.length;
              var tempArray = [];

              for (index = 0; index < arrayLength; index += chunkSize) {
                tempArray.push((0, _slice["default"])(input).call(input, index, index + chunkSize));
              }

              return tempArray;
            };

            CHUNK_SIZE = 10;
            chunks = chunkArray((0, _filter["default"])(assets).call(assets, function (asset) {
              return asset.name.match(/(system-part|bootloader)/);
            }), CHUNK_SIZE);
            _context7.next = 5;
            return (0, _reduce["default"])(chunks).call(chunks, function (promise, chunk) {
              return promise.then(function (results) {
                var _context6;

                return _promise["default"].all((0, _concat["default"])(_context6 = []).call(_context6, (0, _toConsumableArray2["default"])(results || []), (0, _toConsumableArray2["default"])((0, _map["default"])(chunk).call(chunk, function (asset) {
                  return downloadAssetFile(asset);
                }))));
              });
            }, _promise["default"].resolve([]));

          case 5:
            assetFileNames = _context7.sent;
            return _context7.abrupt("return", (0, _filter["default"])(assetFileNames).call(assetFileNames, function (item) {
              return !!item;
            }));

          case 7:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee3);
  }));

  return function downloadFirmwareBinaries(_x3) {
    return _ref3.apply(this, arguments);
  };
}();

var updateSettings = /*#__PURE__*/function () {
  var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(binaryFileNames) {
    var parser, moduleInfos, scriptSettings;
    return _regenerator["default"].wrap(function _callee4$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            parser = new _binaryVersionReader.HalModuleParser();
            _context9.next = 3;
            return _promise["default"].all((0, _map["default"])(binaryFileNames).call(binaryFileNames, function (filename) {
              return new _promise["default"](function (resolve) {
                var _context8;

                parser.parseFile((0, _concat["default"])(_context8 = "".concat(_settings["default"].BINARIES_DIRECTORY, "/")).call(_context8, filename), function (result) {
                  // For some reason all the new modules are dependent on
                  // version 204 but these modules don't actually exist
                  // Use 207 as it's close enough (v0.7.0)
                  // https://github.com/Brewskey/spark-protocol/issues/145
                  if (result.prefixInfo.depModuleVersion === 204) {
                    // eslint-disable-next-line no-param-reassign
                    result.prefixInfo.depModuleVersion = 207;
                  }

                  resolve(_objectSpread(_objectSpread({}, result), {}, {
                    fileBuffer: undefined,
                    filename: filename
                  }));
                });
              });
            }));

          case 3:
            moduleInfos = _context9.sent;
            scriptSettings = (0, _stringify["default"])(moduleInfos, null, 2);

            _fs["default"].writeFileSync(SETTINGS_FILE, scriptSettings);

            console.log('Updated settings');

          case 7:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee4);
  }));

  return function updateSettings(_x4) {
    return _ref4.apply(this, arguments);
  };
}();

var downloadAppBinaries = /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5() {
    var paths, assetsToDownload, _context10, _context11, _context12, currentPath, assets;

    return _regenerator["default"].wrap(function _callee5$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            paths = ['assets/knownApps'];
            assetsToDownload = [];

          case 2:
            if (!paths.length) {
              _context13.next = 11;
              break;
            }

            currentPath = paths.pop(); // eslint-disable-next-line no-await-in-loop

            _context13.next = 6;
            return githubAPI.rest.repos.getContent({
              owner: GITHUB_USER,
              path: currentPath,
              repo: GITHUB_CLI_REPOSITORY
            });

          case 6:
            assets = _context13.sent;
            assetsToDownload.push.apply(assetsToDownload, (0, _toConsumableArray2["default"])((0, _filter["default"])(_context10 = assets.data).call(_context10, function (asset) {
              return asset.type !== 'dir';
            })));
            paths.push.apply(paths, (0, _toConsumableArray2["default"])((0, _map["default"])(_context11 = (0, _filter["default"])(_context12 = assets.data).call(_context12, function (asset) {
              return asset.type !== 'file';
            })).call(_context11, function (asset) {
              return asset.path;
            })));
            _context13.next = 2;
            break;

          case 11:
            return _context13.abrupt("return", _promise["default"].all((0, _map["default"])(assetsToDownload).call(assetsToDownload, function (asset) {
              return downloadBlob(asset);
            })));

          case 12:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee5);
  }));

  return function downloadAppBinaries() {
    return _ref5.apply(this, arguments);
  };
}();

(0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6() {
  var _ref7, releases, assets, downloadedBinaries;

  return _regenerator["default"].wrap(function _callee6$(_context14) {
    while (1) {
      switch (_context14.prev = _context14.next) {
        case 0:
          _context14.prev = 0;

          if (!_fs["default"].existsSync(_settings["default"].BINARIES_DIRECTORY)) {
            _mkdirp["default"].sync(_settings["default"].BINARIES_DIRECTORY);
          }

          if (!_fs["default"].existsSync(FILE_GEN_DIRECTORY)) {
            _mkdirp["default"].sync(FILE_GEN_DIRECTORY);
          }

          _context14.prev = 3;
          _context14.next = 6;
          return downloadAppBinaries();

        case 6:
          _context14.next = 11;
          break;

        case 8:
          _context14.prev = 8;
          _context14.t0 = _context14["catch"](3);
          console.error(_context14.t0);

        case 11:
          _context14.next = 13;
          return githubAPI.paginate(githubAPI.rest.repos.listReleases, {
            owner: GITHUB_USER,
            repo: GITHUB_FIRMWARE_REPOSITORY
          });

        case 13:
          releases = _context14.sent;
          (0, _sort["default"])(releases).call(releases, function (a, b) {
            return a.published_at - b.published_at;
          });
          assets = (0, _concat["default"])(_ref7 = []).apply(_ref7, (0, _toConsumableArray2["default"])((0, _map["default"])(releases).call(releases, function (release) {
            return release.assets;
          })));
          _context14.next = 18;
          return downloadFirmwareBinaries(assets);

        case 18:
          downloadedBinaries = _context14.sent;
          _context14.next = 21;
          return updateSettings(downloadedBinaries);

        case 21:
          console.log('\r\nCompleted Sync');
          _context14.next = 27;
          break;

        case 24:
          _context14.prev = 24;
          _context14.t1 = _context14["catch"](0);
          console.log(_context14.t1);

        case 27:
        case "end":
          return _context14.stop();
      }
    }
  }, _callee6, null, [[0, 24], [3, 8]]);
}))();