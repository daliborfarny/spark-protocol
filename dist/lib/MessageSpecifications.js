"use strict";

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/slicedToArray"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _indexOf = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/index-of"));

var _hogan = _interopRequireDefault(require("hogan.js"));

var _CoapMessage = _interopRequireDefault(require("./CoapMessage"));

var _context;

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context3; _forEachInstanceProperty(_context3 = ownKeys(Object(source), true)).call(_context3, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context4; _forEachInstanceProperty(_context4 = ownKeys(Object(source))).call(_context4, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

var MessageSpecifications = (0, _map["default"])(_context = [['Hello', {
  code: _CoapMessage["default"].Code.POST,
  response: 'Hello',
  type: _CoapMessage["default"].Type.NON,
  uri: 'h'
}], ['KeyChange', {
  code: _CoapMessage["default"].Code.PUT,
  response: 'KeyChanged',
  type: _CoapMessage["default"].Type.CON,
  uri: 'k'
}], ['UpdateBegin', {
  code: _CoapMessage["default"].Code.POST,
  response: 'UpdateReady',
  type: _CoapMessage["default"].Type.CON,
  uri: 'u'
}], ['UpdateAbort', {
  code: _CoapMessage["default"].Code.BAD_REQUEST,
  type: _CoapMessage["default"].Type.NON
}], ['Chunk', {
  code: _CoapMessage["default"].Code.POST,
  response: 'ChunkReceived',
  type: _CoapMessage["default"].Type.CON,
  uri: 'c'
}], ['ChunkMissed', {
  code: _CoapMessage["default"].Code.GET,
  response: 'ChunkMissedAck',
  type: _CoapMessage["default"].Type.CON,
  uri: 'c'
}], ['UpdateDone', {
  code: _CoapMessage["default"].Code.PUT,
  response: null,
  type: _CoapMessage["default"].Type.CON,
  uri: 'u'
}], ['FunctionCall', {
  code: _CoapMessage["default"].Code.POST,
  response: 'FunctionReturn',
  type: _CoapMessage["default"].Type.CON,
  uri: 'f/{{name}}'
}], ['VariableRequest', {
  code: _CoapMessage["default"].Code.GET,
  response: 'VariableValue',
  type: _CoapMessage["default"].Type.CON,
  uri: 'v/{{name}}'
}], ['PrivateEvent', {
  code: _CoapMessage["default"].Code.POST,
  response: null,
  type: _CoapMessage["default"].Type.NON,
  uri: 'E/{{event_name}}'
}], ['PublicEvent', {
  code: _CoapMessage["default"].Code.POST,
  response: null,
  type: _CoapMessage["default"].Type.NON,
  uri: 'e/{{event_name}}'
}], ['Subscribe', {
  code: _CoapMessage["default"].Code.GET,
  response: null,
  type: _CoapMessage["default"].Type.CON,
  uri: 'e/{{event_name}}'
}], ['Describe', {
  code: _CoapMessage["default"].Code.GET,
  response: 'DescribeReturn',
  type: _CoapMessage["default"].Type.CON,
  uri: 'd'
}], ['GetTime', {
  code: _CoapMessage["default"].Code.GET,
  response: 'GetTimeReturn',
  type: _CoapMessage["default"].Type.CON,
  uri: 't'
}], ['SignalStart', {
  code: _CoapMessage["default"].Code.PUT,
  response: 'SignalStartReturn',
  type: _CoapMessage["default"].Type.CON,
  uri: 's'
}], // 'PrivateSubscribe': {
//   code: CoapMessage.Code.GET,
//   uri: 'E/{{event_name}}',
//   type: CoapMessage.Type.NON,
//   response: null
// },
['EventAck', {
  code: _CoapMessage["default"].Code.EMPTY,
  response: null,
  type: _CoapMessage["default"].Type.ACK,
  uri: null
}], ['EventSlowdown', {
  code: _CoapMessage["default"].Code.BAD_REQUEST,
  response: null,
  type: _CoapMessage["default"].Type.ACK,
  uri: null
}], ['SubscribeAck', {
  code: _CoapMessage["default"].Code.EMPTY,
  response: null,
  type: _CoapMessage["default"].Type.ACK,
  uri: null
}], ['SubscribeFail', {
  code: _CoapMessage["default"].Code.BAD_REQUEST,
  response: null,
  type: _CoapMessage["default"].Type.ACK,
  uri: null
}], ['GetTimeReturn', {
  code: _CoapMessage["default"].Code.CONTENT,
  type: _CoapMessage["default"].Type.ACK
}], ['SignalStartReturn', {
  code: _CoapMessage["default"].Code.CHANGED,
  type: _CoapMessage["default"].Type.ACK
}], ['ChunkMissedAck', {
  code: _CoapMessage["default"].Code.EMPTY,
  type: _CoapMessage["default"].Type.ACK
}], ['DescribeReturn', {
  code: _CoapMessage["default"].Code.CHANGED,
  type: _CoapMessage["default"].Type.NON
}], ['KeyChanged', {
  code: _CoapMessage["default"].Code.CHANGED,
  type: _CoapMessage["default"].Type.NON
}], ['UpdateReady', {
  code: _CoapMessage["default"].Code.CHANGED,
  type: _CoapMessage["default"].Type.NON
}], ['ChunkReceived', {
  code: _CoapMessage["default"].Code.CHANGED,
  type: _CoapMessage["default"].Type.NON
}], ['ChunkReceivedError', {
  code: _CoapMessage["default"].Code.BAD_REQUEST,
  type: _CoapMessage["default"].Type.NON
}], ['FunctionReturn', {
  code: _CoapMessage["default"].Code.CHANGED,
  type: _CoapMessage["default"].Type.NON
}], ['FunctionReturnError', {
  code: _CoapMessage["default"].Code.BAD_REQUEST,
  type: _CoapMessage["default"].Type.NON
}], ['VariableValue', {
  code: _CoapMessage["default"].Code.CONTENT,
  type: _CoapMessage["default"].Type.ACK
}], ['VariableValueError', {
  code: _CoapMessage["default"].Code.BAD_REQUEST,
  type: _CoapMessage["default"].Type.NON
}], ['Ping', {
  code: _CoapMessage["default"].Code.EMPTY,
  type: _CoapMessage["default"].Type.CON
}], ['PingAck', {
  code: _CoapMessage["default"].Code.EMPTY,
  response: null,
  type: _CoapMessage["default"].Type.ACK,
  uri: null
}], ['SocketPing', {
  code: _CoapMessage["default"].Code.EMPTY,
  type: _CoapMessage["default"].Type.NON
}]]).call(_context, function (_ref) {
  var _context2;

  var _ref2 = (0, _slicedToArray2["default"])(_ref, 2),
      name = _ref2[0],
      value = _ref2[1];

  var template = null;

  if (value && value.uri && (0, _indexOf["default"])(_context2 = value.uri).call(_context2, '{') >= 0) {
    template = _hogan["default"].compile(value.uri);
  }

  return [name, _objectSpread(_objectSpread({}, value), {}, {
    template: template
  })];
});
var _default = MessageSpecifications;
exports["default"] = _default;