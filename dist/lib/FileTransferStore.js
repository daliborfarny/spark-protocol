"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;
var _default = {
  APPLICATION: 128,
  FIRMWARE: 0,
  SYSTEM: 1 // storage provided by the platform, e.g. external flash

};
exports["default"] = _default;