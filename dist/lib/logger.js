"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _bunyan = _interopRequireDefault(require("bunyan"));

var _path = _interopRequireDefault(require("path"));

var _settings = _interopRequireDefault(require("../settings"));

var Logger = /*#__PURE__*/function () {
  function Logger() {
    (0, _classCallCheck2["default"])(this, Logger);
  }

  (0, _createClass2["default"])(Logger, null, [{
    key: "createLogger",
    value: function createLogger(applicationName) {
      return _bunyan["default"].createLogger({
        level: _settings["default"].LOG_LEVEL,
        name: applicationName,
        serializers: _bunyan["default"].stdSerializers
      });
    }
  }, {
    key: "createModuleLogger",
    value: function createModuleLogger(applicationModule) {
      return _bunyan["default"].createLogger({
        level: _settings["default"].LOG_LEVEL,
        name: _path["default"].basename(applicationModule.filename),
        serializers: _bunyan["default"].stdSerializers
      });
    }
  }]);
  return Logger;
}();

exports["default"] = Logger;