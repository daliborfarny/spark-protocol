"use strict";

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/slicedToArray"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/typeof"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _indexOf = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/index-of"));

var _filter = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/filter"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _find = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/find"));

var _parseFloat2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/parse-float"));

var _some = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/some"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _forEach = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/for-each"));

var _getOwnPropertyNames = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-names"));

var _map2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/map"));

var _coapPacket = _interopRequireDefault(require("coap-packet"));

var _compactArray = _interopRequireDefault(require("compact-array"));

var _CoapMessage = _interopRequireDefault(require("./CoapMessage"));

var _MessageSpecifications = _interopRequireDefault(require("./MessageSpecifications"));

var _logger = _interopRequireDefault(require("./logger"));

var _context11;

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty2(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context12; _forEachInstanceProperty2(_context12 = ownKeys(Object(source), true)).call(_context12, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context13; _forEachInstanceProperty2(_context13 = ownKeys(Object(source))).call(_context13, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

var logger = _logger["default"].createModuleLogger(module);

var _getRouteKey = function _getRouteKey(code, path) {
  var uri = code + path;
  var idx = (0, _indexOf["default"])(uri).call(uri, '/'); // this assumes all the messages are one character for now.
  // if we wanted to change this, we'd need to find the first non message char,
  // '/' or '?', or use the real coap parsing stuff

  return uri.substr(0, idx + 2);
};

var _messageTypeToPacketProps = function _messageTypeToPacketProps(type) {
  var output = {
    ack: false,
    confirmable: false,
    reset: false
  };
  var types = _CoapMessage["default"].Type;

  if (type === types.ACK) {
    output.ack = true;
  } else if (type === types.CON) {
    output.confirmable = true;
  } else if (type === types.RST) {
    output.reset = true;
  }

  return output;
};

var _decodeNumericValue = function _decodeNumericValue(buffer) {
  var length = buffer.length;

  if (length === 0) {
    return 0;
  }

  if (length === 1) {
    return buffer[0];
  }

  if (length === 2) {
    return buffer.readUInt16BE(0);
  }

  if (length === 3) {
    /* eslint-disable no-bitwise */
    return buffer[1] << 8 | buffer[2] + (buffer[0] << 16 >>> 0);
    /* eslint-enable no-bitwise */
  }

  return buffer.readUInt32BE(0);
};

var CoapMessages = /*#__PURE__*/function () {
  function CoapMessages() {
    (0, _classCallCheck2["default"])(this, CoapMessages);
  }

  (0, _createClass2["default"])(CoapMessages, null, [{
    key: "getUriPath",
    value: // Maps CODE + URL to MessageNames as they appear in 'Spec'
    function getUriPath(packet) {
      var _context;

      var options = (0, _filter["default"])(_context = packet.options || []).call(_context, function (item) {
        return item.name === _CoapMessage["default"].Option.URI_PATH;
      });

      if (!options.length) {
        return '';
      }

      return "/".concat((0, _map["default"])(options).call(options, function (item) {
        return item.value.toString('utf8');
      }).join('/'));
    }
  }, {
    key: "getUriQuery",
    value: function getUriQuery(packet) {
      var _context2, _context3;

      return (0, _map["default"])(_context2 = (0, _filter["default"])(_context3 = packet.options || []).call(_context3, function (item) {
        return item.name === _CoapMessage["default"].Option.URI_QUERY;
      })).call(_context2, function (item) {
        return item.value.toString('utf8');
      }).join('&');
    }
  }, {
    key: "getMaxAge",
    value: function getMaxAge(packet) {
      var _context4;

      var option = (0, _find["default"])(_context4 = packet.options || []).call(_context4, function (item) {
        return item.name === _CoapMessage["default"].Option.MAX_AGE;
      });

      if (!option) {
        return 0;
      }

      return _decodeNumericValue(option.value);
    }
  }, {
    key: "getRequestType",
    value: function getRequestType(packet) {
      var uri = _getRouteKey(packet.code, CoapMessages.getUriPath(packet));

      return CoapMessages._routes.get(uri);
    }
  }, {
    key: "getResponseType",
    value: function getResponseType(name) {
      var specification = CoapMessages._specifications.get(name);

      return specification ? specification.response : null;
    }
  }, {
    key: "statusIsOkay",
    value: function statusIsOkay(message) {
      return (0, _parseFloat2["default"])(message.code) < _CoapMessage["default"].Code.BAD_REQUEST;
    }
  }, {
    key: "isNonTypeMessage",
    value: function isNonTypeMessage(messageName) {
      var specification = CoapMessages._specifications.get(messageName);

      if (!specification) {
        return false;
      }

      return specification.type === _CoapMessage["default"].Type.NON;
    }
  }, {
    key: "wrap",
    value: function wrap(messageName, messageId, params, options, data, token) {
      try {
        var _context6, _context9;

        var specification = CoapMessages._specifications.get(messageName);

        if (!specification) {
          logger.error({
            err: new Error(messageName),
            messageName: messageName
          }, 'Unknown Message Type');
          return null;
        } // Format our url


        var uri = specification.uri;
        var queryParams = [];

        if (params) {
          var _context5;

          if (specification.template) {
            uri = specification.template.render(params);
          }

          queryParams = (0, _map["default"])(_context5 = params.args || []).call(_context5, function (value) {
            var newValue = null;

            if (Buffer.isBuffer(value)) {
              newValue = value;
            } else if (typeof value === 'number') {
              newValue = Buffer.alloc(value);
            } else {
              newValue = Buffer.from(value);
            }

            return {
              name: _CoapMessage["default"].Option.URI_QUERY,
              value: newValue
            };
          });
        }

        var uriOptions = [];
        var hasExistingUri = (0, _some["default"])(_context6 = options || []).call(_context6, function (item) {
          return item.name === _CoapMessage["default"].Option.URI_PATH;
        });

        if (uri && !hasExistingUri) {
          var _context7, _context8;

          uriOptions = (0, _map["default"])(_context7 = (0, _filter["default"])(_context8 = uri.split('/')).call(_context8, function (segment) {
            return !!segment;
          })).call(_context7, function (segment) {
            return {
              name: _CoapMessage["default"].Option.URI_PATH,
              value: Buffer.from(segment)
            };
          });
        }

        return _coapPacket["default"].generate(_objectSpread(_objectSpread({}, _messageTypeToPacketProps(specification.type)), {}, {
          code: specification.code.toString(),
          messageId: messageId,
          options: (0, _compactArray["default"])((0, _concat["default"])(_context9 = []).call(_context9, (0, _toConsumableArray2["default"])(uriOptions), (0, _toConsumableArray2["default"])(options || []), (0, _toConsumableArray2["default"])(queryParams))),
          payload: data || Buffer.alloc(0)
        }, token || token === 0 ? {
          token: Buffer.from([token])
        } : {}));
      } catch (error) {
        logger.error({
          err: error,
          messageName: messageName
        }, 'Coap Error');
      }

      return null;
    }
  }, {
    key: "unwrap",
    value: function unwrap(data) {
      if (!data) {
        return null;
      }

      try {
        return _coapPacket["default"].parse(data);
      } catch (error) {
        logger.error({
          data: data,
          err: error
        }, 'Coap Error');
      }

      return null;
    } // http://en.wikipedia.org/wiki/X.690
    // === TYPES: SUBSET OF ASN.1 TAGS ===
    //
    // 1: BOOLEAN (false=0, true=1)
    // 2: INTEGER (int32)
    // 4: OCTET STRING (arbitrary bytes)
    // 5: NULL (void for return value only)
    // 9: REAL (double)
    // Translates the integer variable type enum to user friendly string types

  }, {
    key: "translateIntTypes",
    value: function translateIntTypes(varState) {
      var _context10;

      if (!varState) {
        return null;
      }

      var translatedVarState = {};
      (0, _forEach["default"])(_context10 = (0, _getOwnPropertyNames["default"])(varState)).call(_context10, function (varName) {
        var intType = varState && varState[varName];

        if (typeof intType === 'number') {
          var str = CoapMessages.getNameFromTypeInt(intType);

          if (str !== null) {
            translatedVarState[varName] = str;
          }
        }
      });
      return _objectSpread(_objectSpread({}, varState), translatedVarState);
    }
  }, {
    key: "getNameFromTypeInt",
    value: function getNameFromTypeInt(typeInt) {
      switch (typeInt) {
        case 1:
          {
            return 'bool';
          }

        case 2:
          {
            return 'int32';
          }

        case 4:
          {
            return 'string';
          }

        case 5:
          {
            return 'null';
          }

        case 9:
          {
            return 'double';
          }

        default:
          {
            logger.error({
              err: new Error('asked for unknown type'),
              typeInt: typeInt
            }, 'asked for unknown type');
            throw new Error("error getNameFromTypeInt: ".concat(typeInt));
          }
      }
    } // eslint-disable-next-line func-names

  }, {
    key: "tryFromBinary",
    value: function tryFromBinary(buffer, typeName) {
      var result = null;

      try {
        result = CoapMessages.fromBinary(buffer, typeName);
      } catch (error) {
        logger.error({
          buffer: buffer.toString(),
          err: error,
          typeName: typeName
        }, 'Could not parse type');
      }

      return result;
    } // eslint-disable-next-line func-names

  }, {
    key: "fromBinary",
    value: function fromBinary(buffer, typeName) {
      switch (typeName.toLowerCase()) {
        case 'bool':
          {
            return !!buffer.readUInt8(0);
          }

        case 'byte':
          {
            return buffer.readUInt8(0);
          }

        case 'crc':
          {
            return buffer.readInt32BE(0);
          }

        case 'uint32':
          {
            return buffer.readUInt32BE(0);
          }

        case 'uint16':
          {
            return buffer.readUInt16BE(0);
          }

        case 'int':
        case 'int32':
        case 'number':
          {
            if (!buffer.length) {
              return 0;
            }

            return buffer.readIntBE(0, Math.min(4, buffer.length));
          }

        case 'float':
          {
            return buffer.readFloatBE(0);
          }

        case 'double':
          {
            // doubles on the device are little-endian
            return buffer.readDoubleLE(0);
          }

        case 'buffer':
          {
            return buffer;
          }

        case 'string':
        default:
          {
            return buffer.toString('utf8');
          }
      }
    }
  }, {
    key: "toBinary",
    value: function toBinary(value, typeName) {
      // eslint-disable-next-line no-param-reassign
      typeName = typeName || (0, _typeof2["default"])(value);

      if (value === null) {
        return Buffer.alloc(0);
      }

      switch (typeName) {
        case 'uint8':
          {
            var buffer = Buffer.allocUnsafe(1);
            buffer.writeUInt8(value, 0);
            return buffer;
          }

        case 'uint16':
          {
            var _buffer = Buffer.allocUnsafe(2);

            _buffer.writeUInt16BE(value, 0);

            return _buffer;
          }

        case 'uint32':
        case 'crc':
          {
            var _buffer2 = Buffer.allocUnsafe(4);

            _buffer2.writeUInt32BE(value, 0);

            return _buffer2;
          }

        case 'int32':
          {
            var _buffer3 = Buffer.allocUnsafe(4);

            _buffer3.writeInt32BE(value, 0);

            return _buffer3;
          }

        case 'number':
        case 'double':
          {
            var _buffer4 = Buffer.allocUnsafe(4);

            _buffer4.writeDoubleLE(value, 0);

            return _buffer4;
          }

        case 'buffer':
          {
            return (0, _concat["default"])(Buffer).call(Buffer, [value]);
          }

        case 'string':
        default:
          {
            return Buffer.from(value || '');
          }
      }
    }
  }]);
  return CoapMessages;
}();

(0, _defineProperty2["default"])(CoapMessages, "_specifications", new _map2["default"](_MessageSpecifications["default"]));
(0, _defineProperty2["default"])(CoapMessages, "_routes", new _map2["default"]((0, _map["default"])(_context11 = (0, _filter["default"])(_MessageSpecifications["default"]).call(_MessageSpecifications["default"], // eslint-disable-next-line no-unused-vars
function (_ref) {
  var _ref2 = (0, _slicedToArray2["default"])(_ref, 2),
      name = _ref2[0],
      value = _ref2[1];

  return !!value.uri;
})).call(_context11, function (_ref3) {
  var _ref4 = (0, _slicedToArray2["default"])(_ref3, 2),
      name = _ref4[0],
      value = _ref4[1];

  // see what it looks like without params
  var uri = value.template ? value.template.render({}) : value.uri;

  var routeKey = _getRouteKey(value.code, "/".concat(uri || ''));

  return [routeKey, name];
})));
var _default = CoapMessages;
exports["default"] = _default;