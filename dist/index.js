"use strict";

var _typeof = require("@babel/runtime-corejs3/helpers/typeof");

var _WeakMap = require("@babel/runtime-corejs3/core-js-stable/weak-map");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

_Object$defineProperty(exports, "ClaimCodeManager", {
  enumerable: true,
  get: function get() {
    return _ClaimCodeManager["default"];
  }
});

_Object$defineProperty(exports, "Device", {
  enumerable: true,
  get: function get() {
    return _Device["default"];
  }
});

_Object$defineProperty(exports, "DeviceAttributeFileRepository", {
  enumerable: true,
  get: function get() {
    return _DeviceAttributeFileRepository["default"];
  }
});

_Object$defineProperty(exports, "DeviceKeyFileRepository", {
  enumerable: true,
  get: function get() {
    return _DeviceKeyFileRepository["default"];
  }
});

_Object$defineProperty(exports, "DeviceServer", {
  enumerable: true,
  get: function get() {
    return _DeviceServer["default"];
  }
});

_Object$defineProperty(exports, "EventPublisher", {
  enumerable: true,
  get: function get() {
    return _EventPublisher["default"];
  }
});

_Object$defineProperty(exports, "FileManager", {
  enumerable: true,
  get: function get() {
    return _FileManager["default"];
  }
});

_Object$defineProperty(exports, "FirmwareSettings", {
  enumerable: true,
  get: function get() {
    return _settings2.FirmwareSettings;
  }
});

_Object$defineProperty(exports, "JSONFileManager", {
  enumerable: true,
  get: function get() {
    return _JSONFileManager["default"];
  }
});

_Object$defineProperty(exports, "SPARK_SERVER_EVENTS", {
  enumerable: true,
  get: function get() {
    return _SparkServerEvents["default"];
  }
});

_Object$defineProperty(exports, "ServerKeyFileRepository", {
  enumerable: true,
  get: function get() {
    return _ServerKeyFileRepository["default"];
  }
});

_Object$defineProperty(exports, "defaultBindings", {
  enumerable: true,
  get: function get() {
    return _defaultBindings["default"];
  }
});

_Object$defineProperty(exports, "memoizeGet", {
  enumerable: true,
  get: function get() {
    return _memoizeGet["default"];
  }
});

_Object$defineProperty(exports, "memoizeSet", {
  enumerable: true,
  get: function get() {
    return _memoizeSet["default"];
  }
});

exports.settings = void 0;

var _DeviceAttributeFileRepository = _interopRequireDefault(require("./repository/DeviceAttributeFileRepository"));

var _DeviceKeyFileRepository = _interopRequireDefault(require("./repository/DeviceKeyFileRepository"));

var _ClaimCodeManager = _interopRequireDefault(require("./lib/ClaimCodeManager"));

var _EventPublisher = _interopRequireDefault(require("./lib/EventPublisher"));

var _DeviceServer = _interopRequireDefault(require("./server/DeviceServer"));

var _FileManager = _interopRequireDefault(require("./repository/FileManager"));

var _JSONFileManager = _interopRequireDefault(require("./repository/JSONFileManager"));

var _ServerKeyFileRepository = _interopRequireDefault(require("./repository/ServerKeyFileRepository"));

var _Device = _interopRequireDefault(require("./clients/Device"));

var settings = _interopRequireWildcard(require("./settings"));

exports.settings = settings;

var _settings2 = require("../third-party/settings.json");

var _defaultBindings = _interopRequireDefault(require("./defaultBindings"));

var _memoizeGet = _interopRequireDefault(require("./decorators/memoizeGet"));

var _memoizeSet = _interopRequireDefault(require("./decorators/memoizeSet"));

var _SparkServerEvents = _interopRequireDefault(require("./lib/SparkServerEvents"));

function _getRequireWildcardCache(nodeInterop) { if (typeof _WeakMap !== "function") return null; var cacheBabelInterop = new _WeakMap(); var cacheNodeInterop = new _WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = _Object$defineProperty && _Object$getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? _Object$getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { _Object$defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }