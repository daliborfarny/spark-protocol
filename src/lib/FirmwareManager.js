// @flow

import fs from 'fs';
import {
  HalDependencyResolver,
  HalDescribeParser,
  FirmwareModule,
} from 'binary-version-reader';
import nullthrows from 'nullthrows';
import protocolSettings from '../settings';
import FirmwareSettings from '../../third-party/settings.json';
import Logger from './logger';
const logger = Logger.createModuleLogger(module);

type FirmwarePrefixInfo = {
  moduleStartAddy: string,
  moduleEndAddy: string,
  reserved: number,
  moduleFlags: number,
  moduleVersion: number,
  platformID: number,
  moduleFunction: number,
  moduleIndex: number,
  depModuleFunction: number,
  depModuleIndex: number,
  depModuleVersion: number,
  dep2ModuleFunction: number,
  dep2ModuleIndex: number,
  dep2ModuleVersion: number,
  prefixOffset: number,
};

type FirmwareSetting = {
  filename: string,
  crc: {
    ok: boolean,
    storedCrc: string,
    actualCrc: string,
  },
  prefixInfo: FirmwarePrefixInfo,
  suffixInfo: {
    productId: number,
    productVersion: number,
    fwUniqueId: string,
    reserved: number,
    suffixSize: number,
    crcBlock: string,
  },
};

type Dependency = {
  s: number,
  l: string,
  vc: number,
  vv: number,
  f: string,
  n: string,
  v: number,
  d: Array<SubDependency>,
};

type SubDependency = { f: string, n: string, v: number };

const NUMBER_BY_FUNCTION = {
  b: 2,
  s: 4,
  u: 5,
};
const FUNC_BY_NUMBER = {
  2: 'b',
  4: 's',
  5: 'u',
};

export type OTAUpdate = {
  allModuleFunctions: Array<number>,
  allModuleIndices: Array<number>,
  systemFile: Buffer,
  allUpdateFiles: Array<string>,
};

class FirmwareManager {
  static getMissingModules(systemInformation: Object): FirmwareSetting[] {
    return FirmwareManager._getMissingModule(systemInformation);
  }

  static async getOtaSystemUpdateConfig(
    systemInformation: Object,
  ): Promise<?OTAUpdate> {
    const missingDependencies = FirmwareManager._getMissingModule(
      systemInformation,
    );
    console.log('missingDependencies', 'missingDependencies');
    if (!missingDependencies.length) {
      return null;
    }
    const { systemFiles, ...result } = missingDependencies.reduce(
      (acc, dependency) => {
        const dependencyPath = `${protocolSettings.BINARIES_DIRECTORY}/${dependency.filename}`;

        if (!fs.existsSync(dependencyPath)) {
          logger.error(
            {
              dependencyPath,
              dependency,
            },
            'Dependency does not exist on disk',
          );
          return acc;
        }

        const systemFile = fs.readFileSync(dependencyPath);
        return {
          allModuleFunctions: [
            ...acc.allModuleFunctions,
            FUNC_BY_NUMBER[dependency.prefixInfo.moduleFunction],
          ],
          allModuleIndices: [
            ...acc.allModuleIndices,
            dependency.prefixInfo.moduleIndex,
          ],
          systemFiles: [...acc.systemFiles, systemFile],
          allUpdateFiles: [...acc.allUpdateFiles, dependency.filename],
        };
      },
      {
        allModuleFunctions: [],
        allModuleIndices: [],
        systemFiles: [],
        allUpdateFiles: [],
      },
    );

    return {
      ...result,
      systemFile: systemFiles[0],
    };
  }

  static getAppModule(systemInformation: Object): Object {
    const parser = new HalDescribeParser();
    return nullthrows(
      parser
        .getModules(systemInformation)
        // Filter so we only have the app modules
        .find((module: Object): boolean => module.func === 'u'),
    );
  }

  static _getMissingModule(systemInformation: Object): ?(FirmwareSetting[]) {
    const platformID = systemInformation.p;

    // Check CRC to see if there are any bad modules
    const parser = new HalDescribeParser();
    let knownMissingDependencies = parser
      .getModules(systemInformation)
      .filter(module => !module.hasIntegrity())
      .map(module => module.toDescribe());

    if (knownMissingDependencies.length) {
      logger.info('Bad CRC for firmware');
    } else {
      // find missing dependencies
      const dr = new HalDependencyResolver();
      knownMissingDependencies = dr.findAnyMissingDependencies(
        systemInformation,
      );
    }

    if (!knownMissingDependencies.length) {
      return [];
    }

    const findSettingForFirmwareModule = (
      dependency: FirmwareModule,
    ): FirmwareSetting =>
      FirmwareSettings.find(
        ({ prefixInfo }: { prefixInfo: FirmwarePrefixInfo }): boolean =>
          prefixInfo.platformID === platformID &&
          prefixInfo.moduleVersion === dependency.version &&
          prefixInfo.moduleFunction === NUMBER_BY_FUNCTION[dependency.func] &&
          prefixInfo.moduleIndex === parseInt(dependency.name, 10),
      );

    const addRealDependencies = (
      dependency: FirmwareModule,
    ): FirmwareModule => {
      const setting = findSettingForFirmwareModule(dependency);
      if (!setting) {
        logger.error('Cannot find firmware for module', {
          systemInformation,
          dependency,
        });
        return null;
      }

      const result = new FirmwareModule({
        ...(dependency.toDescribe(): Dependency),
        d: [
          {
            f: FUNC_BY_NUMBER[setting.prefixInfo.depModuleFunction],
            n: setting.prefixInfo.depModuleIndex.toString(),
            v: setting.prefixInfo.depModuleVersion,
          },
          {
            f: FUNC_BY_NUMBER[setting.prefixInfo.dep2ModuleFunction],
            n: setting.prefixInfo.dep2ModuleIndex.toString(),
            v: setting.prefixInfo.dep2ModuleVersion,
          },
        ].filter(({ v }: SubDependency) => v !== 0),
      });
      result.filename = setting.filename;
      return result;
    };

    const allMissingDependencies = new Map<string, FirmwareModule>();
    const setDependency = (dep: FirmwareModule) => {
      const key = `${dep.func}#${dep.name}`;
      if (allMissingDependencies.has(key)) {
        const currentDep = allMissingDependencies.get(key);
        if (currentDep.version > dep.version) {
          return;
        }
      }

      allMissingDependencies.set(key, dep);
    };

    const results: FirmwareModule[] = knownMissingDependencies
      .map(dep => addRealDependencies(new FirmwareModule(dep)))
      .filter(Boolean);

    results.forEach(setDependency);
    while (results.length) {
      const result = results.pop();
      const unmetDependencies = [];
      if (!result.areDependenciesMet(systemInformation.m, unmetDependencies)) {
        const settingsModules = unmetDependencies
          .map(addRealDependencies)
          .filter(Boolean);
        results.push(...settingsModules);
        settingsModules.forEach(setDependency);
      }
    }

    // Map dependencies to firmware metadata
    const knownMissingFirmwares: Array<FirmwareSetting> = Array.from(
      allMissingDependencies.values(),
    )
      .sort((a, b) => {
        if (a.func === 'b') {
          return 1;
        }
        return a.name - b.name;
      })
      .map((dep: FirmwareModule): FirmwareSetting => {
        const setting = findSettingForFirmwareModule(dep);

        if (!setting) {
          logger.error({ dep, platformID }, 'Missing firmware setting');
        }

        return setting;
      })
      .filter(Boolean);

    logger.info(
      {
        allFileNames: knownMissingFirmwares.map(firmware => firmware.filename),
      },
      'Discovering missing firmware',
    );

    return knownMissingFirmwares;
  }

  getKnownAppFileName(): ?string {
    throw new Error('getKnownAppFileName has not been implemented.');
  }
}

export default FirmwareManager;
