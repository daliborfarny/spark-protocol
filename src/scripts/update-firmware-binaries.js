#! /usr/bin/env node
// @flow

import fs from 'fs';
import path from 'path';
import { Octokit } from '@octokit/rest';
import mkdirp from 'mkdirp';
import { HalModuleParser } from 'binary-version-reader';
import dotenv from 'dotenv';
import settings from '../settings';

let fileDirectory = path.resolve(process.env.INIT_CWD);
let filePath = null;

// A counter is a lot safer than a while(true)
let count = 0;
while (count < 20) {
  count += 1;
  filePath = path.join(fileDirectory, '.env');
  console.log('Checking for .env: ', filePath);
  if (fs.existsSync(filePath)) {
    break;
  }

  const newFileDirectory = path.join(fileDirectory, '..');
  if (newFileDirectory === fileDirectory) {
    filePath = null;
    break;
  }
  fileDirectory = newFileDirectory;
}

if (!filePath) {
  dotenv.config();
} else {
  dotenv.config({
    path: filePath,
  });
}

const GITHUB_USER = 'particle-iot';
const GITHUB_FIRMWARE_REPOSITORY = 'firmware';
const GITHUB_CLI_REPOSITORY = 'particle-cli';
const FILE_GEN_DIRECTORY = path.join(__dirname, '../../third-party/');
const SETTINGS_FILE = `${FILE_GEN_DIRECTORY}settings.json`;

type Asset = {
  browser_download_url: string,
  id: string,
  name: string,
};

const { GITHUB_AUTH_TOKEN } = process.env;

if (!GITHUB_AUTH_TOKEN) {
  throw new Error(
    'You need to set up a .env file with auth credentials (read public repos)',
  );
}

const githubAPI = new Octokit({
  auth: GITHUB_AUTH_TOKEN,
});

const downloadAssetFile = async (asset: Asset): Promise<*> => {
  const filename = asset.name;
  const fileWithPath = `${settings.BINARIES_DIRECTORY}/${filename}`;
  if (fs.existsSync(fileWithPath)) {
    console.log(`File Exists: ${filename}`);
    return filename;
  }

  console.log(`Downloading ${filename}...`);

  return githubAPI.rest.repos
    .getReleaseAsset({
      headers: {
        accept: 'application/octet-stream',
      },
      asset_id: asset.id,
      owner: GITHUB_USER,
      repo: GITHUB_FIRMWARE_REPOSITORY,
    })
    .then((response: any): string => {
      fs.writeFileSync(fileWithPath, Buffer.from(response.data));
      return filename;
    })
    .catch((error: Error): void =>
      console.error(`Could not download ${asset.name} -- ${error.message}`),
    );
};

const downloadBlob = async (asset: any): Promise<*> => {
  const filename = asset.name;
  const fileWithPath = `${settings.BINARIES_DIRECTORY}/${filename}`;
  if (fs.existsSync(fileWithPath)) {
    console.log(`File Exists: ${filename}`);
    return filename;
  }

  console.log(`Downloading ${filename}...`);

  return githubAPI.rest.git
    .getBlob({
      file_sha: asset.sha,
      headers: {
        accept: 'application/vnd.github.v3.raw',
      },
      owner: GITHUB_USER,
      repo: GITHUB_CLI_REPOSITORY,
    })
    .then((response: any): string => {
      fs.writeFileSync(fileWithPath, Buffer.from(response.data));
      return filename;
    })
    .catch((error: Error): void => console.error(error));
};

const downloadFirmwareBinaries = async (
  assets: Array<Asset>,
): Promise<Array<string>> => {
  const CHUNK_SIZE = 10;

  function chunkArray(
    input: Array<Object>,
    chunkSize: number,
  ): Array<Array<Object>> {
    let index = 0;
    const arrayLength = input.length;
    const tempArray = [];

    for (index = 0; index < arrayLength; index += chunkSize) {
      tempArray.push(input.slice(index, index + chunkSize));
    }

    return tempArray;
  }

  const chunks = chunkArray(
    assets.filter((asset: Object): Promise<string> =>
      asset.name.match(/(system-part|bootloader)/),
    ),
    CHUNK_SIZE,
  );

  const assetFileNames = await chunks.reduce(
    (promise, chunk) =>
      promise.then(results =>
        Promise.all([
          ...(results || []),
          ...chunk.map(asset => downloadAssetFile(asset)),
        ]),
      ),
    Promise.resolve([]),
  );

  return assetFileNames.filter((item: ?string): boolean => !!item);
};

const updateSettings = async (binaryFileNames: Array<string>) => {
  const parser = new HalModuleParser();

  const moduleInfos = await Promise.all(
    binaryFileNames.map(
      (filename: string): Promise<any> =>
        new Promise((resolve: (result: any) => void) => {
          parser.parseFile(
            `${settings.BINARIES_DIRECTORY}/${filename}`,
            (result: any) => {
              // For some reason all the new modules are dependent on
              // version 204 but these modules don't actually exist
              // Use 207 as it's close enough (v0.7.0)
              // https://github.com/Brewskey/spark-protocol/issues/145
              if (result.prefixInfo.depModuleVersion === 204) {
                // eslint-disable-next-line no-param-reassign
                result.prefixInfo.depModuleVersion = 207;
              }

              resolve({
                ...result,
                fileBuffer: undefined,
                filename,
              });
            },
          );
        }),
    ),
  );

  const scriptSettings = JSON.stringify(moduleInfos, null, 2);

  fs.writeFileSync(SETTINGS_FILE, scriptSettings);
  console.log('Updated settings');
};

const downloadAppBinaries = async (): Promise<*> => {
  const paths = ['assets/knownApps'];
  const assetsToDownload = [];

  while (paths.length) {
    const currentPath = paths.pop();

    // eslint-disable-next-line no-await-in-loop
    const assets = await githubAPI.rest.repos.getContent({
      owner: GITHUB_USER,
      path: currentPath,
      repo: GITHUB_CLI_REPOSITORY,
    });

    assetsToDownload.push(...assets.data.filter(asset => asset.type !== 'dir'));
    paths.push(
      ...assets.data
        .filter(asset => asset.type !== 'file')
        .map(asset => asset.path),
    );
  }

  return Promise.all(
    assetsToDownload.map((asset: Object): Promise<string> =>
      downloadBlob(asset),
    ),
  );
};

(async (): Promise<*> => {
  try {
    if (!fs.existsSync(settings.BINARIES_DIRECTORY)) {
      mkdirp.sync(settings.BINARIES_DIRECTORY);
    }
    if (!fs.existsSync(FILE_GEN_DIRECTORY)) {
      mkdirp.sync(FILE_GEN_DIRECTORY);
    }

    try {
      // Download app binaries
      await downloadAppBinaries();
    } catch (error) {
      console.error(error);
    }

    // Download firmware binaries
    const releases = await githubAPI.paginate(
      githubAPI.rest.repos.listReleases,
      {
        owner: GITHUB_USER,
        repo: GITHUB_FIRMWARE_REPOSITORY,
      },
    );

    releases.sort(
      (a: Object, b: Object): number => a.published_at - b.published_at,
    );

    const assets = [].concat(
      ...releases.map((release: any): Array<any> => release.assets),
    );

    const downloadedBinaries = await downloadFirmwareBinaries(assets);
    await updateSettings(downloadedBinaries);

    console.log('\r\nCompleted Sync');
  } catch (err) {
    console.log(err);
  }
})();
