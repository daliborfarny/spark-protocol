"use strict";

var _Reflect$construct = require("@babel/runtime-corejs3/core-js-stable/reflect/construct");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/getPrototypeOf"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _stream = require("stream");

var _crypto = _interopRequireDefault(require("crypto"));

var _settings = _interopRequireDefault(require("../settings"));

var _logger = _interopRequireDefault(require("./logger"));

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = _Reflect$construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !_Reflect$construct) return false; if (_Reflect$construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(_Reflect$construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var logger = _logger["default"].createModuleLogger(module);

var CryptoStream = /*#__PURE__*/function (_Transform) {
  (0, _inherits2["default"])(CryptoStream, _Transform);

  var _super = _createSuper(CryptoStream);

  function CryptoStream(options) {
    var _this;

    (0, _classCallCheck2["default"])(this, CryptoStream);
    _this = _super.call(this);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_key", void 0);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_iv", void 0);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_streamType", void 0);
    _this._key = options.key;
    _this._iv = options.iv;
    _this._streamType = options.streamType;
    return _this;
  }

  (0, _createClass2["default"])(CryptoStream, [{
    key: "_transform",
    value: function _transform(chunk, encoding, callback) {
      if (!chunk.length) {
        logger.error({
          encoding: encoding,
          err: new Error("CryptoStream transform error: Chunk didn't have any length")
        });
        callback();
        return;
      }

      try {
        var data = chunk;
        var cipherParams = [_settings["default"].CRYPTO_ALGORITHM, this._key, this._iv];
        var cipher = this._streamType === 'encrypt' ? _crypto["default"].createCipheriv.apply(_crypto["default"], cipherParams) : _crypto["default"].createDecipheriv.apply(_crypto["default"], cipherParams);
        var transformedData = cipher.update(data);
        var extraData = cipher["final"]();
        var output = (0, _concat["default"])(Buffer).call(Buffer, [transformedData, extraData], transformedData.length + extraData.length);
        var ivContainer = this._streamType === 'encrypt' ? output : data;
        this._iv = Buffer.alloc(16);
        ivContainer.copy(this._iv, 0, 0, 16);
        this.push(output);
      } catch (error) {
        logger.error({
          encoding: encoding,
          err: error
        }, 'CryptoStream transform error');
      }

      callback();
    }
  }]);
  return CryptoStream;
}(_stream.Transform);

var _default = CryptoStream;
exports["default"] = _default;