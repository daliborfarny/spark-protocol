"use strict";

var _Reflect$construct = require("@babel/runtime-corejs3/core-js-stable/reflect/construct");

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports.getRequestEventName = exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _concat = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/concat"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/map"));

var _setImmediate2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/set-immediate"));

var _promise = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/promise"));

var _setTimeout2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/set-timeout"));

var _forEach = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/for-each"));

var _filter = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/filter"));

var _startsWith = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/starts-with"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/getPrototypeOf"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _events = _interopRequireDefault(require("events"));

var _uuid = _interopRequireDefault(require("uuid"));

var _settings = _interopRequireDefault(require("../settings"));

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty2(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context8; _forEachInstanceProperty2(_context8 = ownKeys(Object(source), true)).call(_context8, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context9; _forEachInstanceProperty2(_context9 = ownKeys(Object(source))).call(_context9, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = _Reflect$construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !_Reflect$construct) return false; if (_Reflect$construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(_Reflect$construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

var getRequestEventName = function getRequestEventName(eventName) {
  return "".concat(eventName, "/request");
};

exports.getRequestEventName = getRequestEventName;
var LISTEN_FOR_RESPONSE_TIMEOUT = 15000;

var EventPublisher = /*#__PURE__*/function (_EventEmitter) {
  (0, _inherits2["default"])(EventPublisher, _EventEmitter);

  var _super = _createSuper(EventPublisher);

  function EventPublisher() {
    var _context;

    var _this;

    (0, _classCallCheck2["default"])(this, EventPublisher);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, (0, _concat["default"])(_context = [this]).call(_context, args));
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "_subscriptionsByID", new _map["default"]());
    return _this;
  }

  (0, _createClass2["default"])(EventPublisher, [{
    key: "publish",
    value: function publish(eventData, options) {
      var _this2 = this;

      var ttl = eventData.ttl && eventData.ttl > 0 ? eventData.ttl : _settings["default"].DEFAULT_EVENT_TTL;

      var event = _objectSpread(_objectSpread({}, eventData), {}, {
        publishedAt: new Date(),
        isInternal: (options === null || options === void 0 ? void 0 : options.isInternal) || false,
        isPublic: (options === null || options === void 0 ? void 0 : options.isPublic) || false,
        ttl: ttl
      });

      (0, _setImmediate2["default"])(function () {
        _this2._emitWithPrefix(eventData.name, event);

        _this2.emit('*', event);
      });
    }
  }, {
    key: "publishAndListenForResponse",
    value: function () {
      var _publishAndListenForResponse = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(eventData) {
        var _context2,
            _context3,
            _this3 = this;

        var eventID, requestEventName, responseEventName;
        return _regenerator["default"].wrap(function _callee$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                eventID = (0, _uuid["default"])();
                requestEventName = (0, _concat["default"])(_context2 = "".concat(getRequestEventName(eventData.name), "/")).call(_context2, eventID);
                responseEventName = (0, _concat["default"])(_context3 = "".concat(eventData.name, "/response/")).call(_context3, eventID);
                return _context4.abrupt("return", new _promise["default"](function (resolve, reject) {
                  var responseListener = function responseListener(event) {
                    return resolve(event.context || {});
                  };

                  _this3.subscribe(responseEventName, responseListener, {
                    once: true,
                    subscriptionTimeout: LISTEN_FOR_RESPONSE_TIMEOUT,
                    timeoutHandler: function timeoutHandler() {
                      return reject(new Error("Response timeout for event: ".concat(eventData.name)));
                    }
                  });

                  _this3.publish(_objectSpread(_objectSpread({}, eventData), {}, {
                    context: _objectSpread(_objectSpread({}, eventData.context), {}, {
                      responseEventName: responseEventName
                    }),
                    name: requestEventName
                  }), {
                    isInternal: true,
                    isPublic: false
                  });
                }));

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee);
      }));

      function publishAndListenForResponse(_x) {
        return _publishAndListenForResponse.apply(this, arguments);
      }

      return publishAndListenForResponse;
    }()
  }, {
    key: "subscribe",
    value: function subscribe() {
      var _this4 = this;

      var eventNamePrefix = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '*';
      var eventHandler = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      var filterOptions = options.filterOptions,
          once = options.once,
          subscriptionTimeout = options.subscriptionTimeout,
          timeoutHandler = options.timeoutHandler;
      var subscriptionID = (0, _uuid["default"])();

      while (this._subscriptionsByID.has(subscriptionID)) {
        subscriptionID = (0, _uuid["default"])();
      }

      var listener = filterOptions ? this._filterEvents(eventHandler, filterOptions) : eventHandler;

      this._subscriptionsByID.set(subscriptionID, {
        eventNamePrefix: eventNamePrefix,
        id: subscriptionID,
        listener: listener,
        options: options
      });

      if (subscriptionTimeout) {
        var timeout = (0, _setTimeout2["default"])(function () {
          _this4.unsubscribe(subscriptionID);

          if (timeoutHandler) {
            timeoutHandler();
          }
        }, subscriptionTimeout);
        this.once(eventNamePrefix, function () {
          return clearTimeout(timeout);
        });
      }

      if (once) {
        this.once(eventNamePrefix, function (event) {
          _this4._subscriptionsByID["delete"](subscriptionID);

          listener(event);
        });
      } else {
        this.on(eventNamePrefix, listener);
      }

      return subscriptionID;
    }
  }, {
    key: "unsubscribe",
    value: function unsubscribe(subscriptionID) {
      var subscription = this._subscriptionsByID.get(subscriptionID);

      if (!subscription) {
        return;
      }

      this.removeListener(subscription.eventNamePrefix, subscription.listener);

      this._subscriptionsByID["delete"](subscriptionID);
    }
  }, {
    key: "unsubscribeBySubscriberID",
    value: function unsubscribeBySubscriberID(subscriberID) {
      var _context5,
          _this5 = this;

      (0, _forEach["default"])(_context5 = this._subscriptionsByID).call(_context5, function (subscription) {
        if (subscription.options.subscriberID === subscriberID) {
          _this5.unsubscribe(subscription.id);
        }
      });
    }
  }, {
    key: "_emitWithPrefix",
    value: function _emitWithPrefix(eventName, event) {
      var _context6,
          _context7,
          _this6 = this;

      (0, _forEach["default"])(_context6 = (0, _filter["default"])(_context7 = this.eventNames()).call(_context7, function (eventNamePrefix) {
        return (0, _startsWith["default"])(eventName).call(eventName, eventNamePrefix);
      })).call(_context6, function (eventNamePrefix) {
        return _this6.emit(eventNamePrefix, event);
      });
    }
  }, {
    key: "_filterEvents",
    value: function _filterEvents(eventHandler, filterOptions) {
      return function (event) {
        if (event.isInternal && filterOptions.listenToInternalEvents === false) {
          return;
        } // filter private events from another devices


        if (filterOptions.userID && !event.isPublic && filterOptions.userID !== event.userID) {
          return;
        } // filter private events with wrong connectionID


        if (!event.isPublic && filterOptions.connectionID && event.connectionID !== filterOptions.connectionID) {
          return;
        } // filter mydevices events


        if (filterOptions.mydevices && filterOptions.userID !== event.userID) {
          return;
        } // filter event by deviceID


        if (filterOptions.deviceID && event.deviceID !== filterOptions.deviceID) {
          return;
        } // filter broadcasted events


        if (filterOptions.listenToBroadcastedEvents === false && event.broadcasted) {
          return;
        }

        process.nextTick(function () {
          return eventHandler(event);
        });
      };
    }
  }]);
  return EventPublisher;
}(_events["default"]);

var _default = EventPublisher;
exports["default"] = _default;