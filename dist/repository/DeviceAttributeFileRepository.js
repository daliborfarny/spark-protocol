"use strict";

var _Object$keys = require("@babel/runtime-corejs3/core-js-stable/object/keys");

var _Object$getOwnPropertySymbols = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-symbols");

var _filterInstanceProperty2 = require("@babel/runtime-corejs3/core-js-stable/instance/filter");

var _Object$getOwnPropertyDescriptor2 = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor");

var _forEachInstanceProperty = require("@babel/runtime-corejs3/core-js-stable/instance/for-each");

var _Object$getOwnPropertyDescriptors = require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptors");

var _Object$defineProperties = require("@babel/runtime-corejs3/core-js-stable/object/define-properties");

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _filter = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/filter"));

var _find = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/find"));

var _getOwnPropertyDescriptor = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/object/get-own-property-descriptor"));

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var _applyDecoratedDescriptor2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/applyDecoratedDescriptor"));

var _JSONFileManager = _interopRequireDefault(require("./JSONFileManager"));

var _memoizeGet = _interopRequireDefault(require("../decorators/memoizeGet"));

var _memoizeSet = _interopRequireDefault(require("../decorators/memoizeSet"));

var _dec, _dec2, _dec3, _dec4, _dec5, _class;

function ownKeys(object, enumerableOnly) { var keys = _Object$keys(object); if (_Object$getOwnPropertySymbols) { var symbols = _Object$getOwnPropertySymbols(object); if (enumerableOnly) { symbols = _filterInstanceProperty2(symbols).call(symbols, function (sym) { return _Object$getOwnPropertyDescriptor2(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context7; _forEachInstanceProperty(_context7 = ownKeys(Object(source), true)).call(_context7, function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (_Object$getOwnPropertyDescriptors) { _Object$defineProperties(target, _Object$getOwnPropertyDescriptors(source)); } else { var _context8; _forEachInstanceProperty(_context8 = ownKeys(Object(source))).call(_context8, function (key) { _Object$defineProperty(target, key, _Object$getOwnPropertyDescriptor2(source, key)); }); } } return target; }

// getByID, deleteByID and update uses model.deviceID as ID for querying
var DeviceAttributeFileRepository = (_dec = (0, _memoizeSet["default"])(), _dec2 = (0, _memoizeSet["default"])(['deviceID']), _dec3 = (0, _memoizeGet["default"])(['deviceID']), _dec4 = (0, _memoizeGet["default"])(['deviceName']), _dec5 = (0, _memoizeGet["default"])(), (_class = /*#__PURE__*/function () {
  function DeviceAttributeFileRepository(path) {
    (0, _classCallCheck2["default"])(this, DeviceAttributeFileRepository);
    (0, _defineProperty2["default"])(this, "_fileManager", void 0);
    this._fileManager = new _JSONFileManager["default"](path);
  } // eslint-disable-next-line no-unused-vars


  (0, _createClass2["default"])(DeviceAttributeFileRepository, [{
    key: "create",
    value: function () {
      var _create = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(model) {
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                throw new Error('Create device attributes not implemented');

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function create(_x) {
        return _create.apply(this, arguments);
      }

      return create;
    }()
  }, {
    key: "updateByID",
    value: function () {
      var _updateByID = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(deviceID, props) {
        var currentAttributes, modelToSave;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.getByID(deviceID);

              case 2:
                currentAttributes = _context2.sent;

                if (currentAttributes) {
                  _context2.next = 5;
                  break;
                }

                throw new Error("Could not find device with ID ".concat(deviceID));

              case 5:
                modelToSave = _objectSpread(_objectSpread({}, currentAttributes || {}), props);

                this._fileManager.writeFile("".concat(deviceID, ".json"), _objectSpread(_objectSpread({}, modelToSave), {}, {
                  timestamp: new Date()
                }));

                return _context2.abrupt("return", modelToSave);

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function updateByID(_x2, _x3) {
        return _updateByID.apply(this, arguments);
      }

      return updateByID;
    }()
  }, {
    key: "deleteByID",
    value: function () {
      var _deleteByID = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(deviceID) {
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                this._fileManager.deleteFile("".concat(deviceID, ".json"));

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function deleteByID(_x4) {
        return _deleteByID.apply(this, arguments);
      }

      return deleteByID;
    }()
  }, {
    key: "getAll",
    value: function () {
      var _getAll2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4() {
        var userID,
            allData,
            _args4 = arguments;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                userID = _args4.length > 0 && _args4[0] !== undefined ? _args4[0] : null;
                _context4.next = 3;
                return this._getAll();

              case 3:
                allData = _context4.sent;

                if (!userID) {
                  _context4.next = 6;
                  break;
                }

                return _context4.abrupt("return", (0, _filter["default"])(allData).call(allData, function (attributes) {
                  return attributes.ownerID === userID;
                }));

              case 6:
                return _context4.abrupt("return", allData);

              case 7:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function getAll() {
        return _getAll2.apply(this, arguments);
      }

      return getAll;
    }()
  }, {
    key: "getByID",
    value: function () {
      var _getByID = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(deviceID) {
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                return _context5.abrupt("return", this._fileManager.getFile("".concat(deviceID, ".json")));

              case 1:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function getByID(_x5) {
        return _getByID.apply(this, arguments);
      }

      return getByID;
    }()
  }, {
    key: "getByName",
    value: function () {
      var _getByName = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(deviceName) {
        var allData, result;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                allData = this._getAll();
                result = (0, _find["default"])(allData).call(allData, function (attributes) {
                  return attributes.name === deviceName;
                });

                if (result) {
                  _context6.next = 4;
                  break;
                }

                throw new Error("Missing device ".concat(deviceName));

              case 4:
                return _context6.abrupt("return", result);

              case 5:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function getByName(_x6) {
        return _getByName.apply(this, arguments);
      }

      return getByName;
    }()
  }, {
    key: "_getAll",
    value: function _getAll() {
      return this._fileManager.getAllData();
    }
  }]);
  return DeviceAttributeFileRepository;
}(), ((0, _applyDecoratedDescriptor2["default"])(_class.prototype, "updateByID", [_dec], (0, _getOwnPropertyDescriptor["default"])(_class.prototype, "updateByID"), _class.prototype), (0, _applyDecoratedDescriptor2["default"])(_class.prototype, "deleteByID", [_dec2], (0, _getOwnPropertyDescriptor["default"])(_class.prototype, "deleteByID"), _class.prototype), (0, _applyDecoratedDescriptor2["default"])(_class.prototype, "getByID", [_dec3], (0, _getOwnPropertyDescriptor["default"])(_class.prototype, "getByID"), _class.prototype), (0, _applyDecoratedDescriptor2["default"])(_class.prototype, "getByName", [_dec4], (0, _getOwnPropertyDescriptor["default"])(_class.prototype, "getByName"), _class.prototype), (0, _applyDecoratedDescriptor2["default"])(_class.prototype, "_getAll", [_dec5], (0, _getOwnPropertyDescriptor["default"])(_class.prototype, "_getAll"), _class.prototype)), _class));
var _default = DeviceAttributeFileRepository;
exports["default"] = _default;