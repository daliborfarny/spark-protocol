"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs3/core-js-stable/object/define-property");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs3/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

/* eslint-disable no-unused-vars */
// getByID, deleteByID and update uses model.deviceID as ID for querying
var MockProductDeviceRepository = /*#__PURE__*/function () {
  function MockProductDeviceRepository() {
    (0, _classCallCheck2["default"])(this, MockProductDeviceRepository);
  }

  (0, _createClass2["default"])(MockProductDeviceRepository, [{
    key: "create",
    value: function create(model) {
      throw new Error('The method is not implemented');
    }
  }, {
    key: "deleteByID",
    value: function deleteByID(productDeviceID) {
      throw new Error('The method is not implemented');
    } // eslint-disable-next-line

  }, {
    key: "getAll",
    value: function getAll() {
      throw new Error('The method is not implemented');
    }
  }, {
    key: "getByID",
    value: function getByID(productDeviceID) {
      throw new Error('The method is not implemented');
    }
  }, {
    key: "getAllByProductID",
    value: function getAllByProductID(productID, page, perPage) {
      throw new Error('The method is not implemented');
    } // This is here just to make things work when used without spark-server

  }, {
    key: "getFromDeviceID",
    value: function () {
      var _getFromDeviceID = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(deviceID) {
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                return _context.abrupt("return", null);

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getFromDeviceID(_x) {
        return _getFromDeviceID.apply(this, arguments);
      }

      return getFromDeviceID;
    }()
  }, {
    key: "updateByID",
    value: function updateByID(productDeviceID, props) {
      throw new Error('The method is not implemented');
    }
  }]);
  return MockProductDeviceRepository;
}();
/* eslint-enable no-unused-vars */


var _default = MockProductDeviceRepository;
exports["default"] = _default;